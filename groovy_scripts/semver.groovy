
public class Semver implements Comparable {
    //String expectedPrefix="tag_"
    String expectedPrefix=""
    Integer major, minor, patch
    String version
    boolean release
    public init() {
      String tmp = version
        release = tmp.endsWith(".RELEASE")
        tmp = release ? tmp.substring(0,tmp.indexOf(".RELEASE"))
                    : tmp.substring(0,tmp.indexOf("-SNAPSHOT"))
        String[] pieces = tmp.split("\\.")
        major=Integer.parseInt(pieces[0])
        minor=Integer.parseInt(pieces[1])
        patch=Integer.parseInt(pieces[2])    
    }

    public int compareTo(Object other) {
        if (release == other.release) {
            return compareSemver(other)
        } 
        return release ? 1 : -1
        
    }
    private int compareSemver(Semver other) {
        if (major==other.major) {
            if(minor==other.minor) {
                return patch.compareTo(other.patch)
            } else {
                return minor.compareTo(other.minor)
            }
        }
        return major.compareTo(other.major)
    }
    public String toString() {
        return "${version}"
        //return "${version} : ${major}.${minor}.${patch} is release ${release} from ${version}"
    }

}

public void testSemver() {
    sems = ["4.2.0.RELEASE","1.2.3.RELEASE","1.1.4.RELEASE","2.1.0-SNAPSHOT","1.3.0-SNAPSHOT"]
    println("sems:${sems}")
    tss = []
    sems.each {
        t = new Semver()
        t.version = it
        t.init()
        tss+=t
    }
    tss = tss.sort()
    println("tss:${tss}")
}
//testSemver()

return this