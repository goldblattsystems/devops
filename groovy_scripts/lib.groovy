import groovy.sql.Sql
import java.io.Serializable
import java.util.jar.JarFile
import java.lang.StringBuilder
import groovy.transform.Field
import groovy.transform.SourceURI
import java.nio.file.Path
import java.nio.file.Paths
import groovy.json.JsonSlurper


@Field Integer tomcatPort=8080
def setTomcatPort(Integer port) {
  tomcatPort = port
}
def writeLinesToFile(pathToFile, lines) {
  file = new File(pathToFile)
  if (file.exists()) {
    file.delete()
  }
  lines.each {line->file.append(line+"\n")}
  
}
@Field Object productionSqlInstance = null
@Field Object  sqlInstance = null
def createSqlScriptLines(List<String>schemas, String scriptDir, List<String>scripts, List<String> rawSql, int indent=5) {
  
  def lines = []
  def ind = " "*indent
  lines += "${ind}set head off"
  lines += "${ind}set feedback off"
  lines += "${ind}set echo off"
  lines += "${ind}set linesize 256"
  lines += "${ind}set trimspool on"
  lines += "${ind}set pagesize 0"
  lines += "${ind}set long 90000"
  lines += "${ind}set serveroutput on size 1000000"
  lines += "${ind}set linesize 2000"
  lines += "${ind}-- scripts:  ${scripts} ${scripts.size()}"
  lines += "${ind}-- scripts.size():  ${scripts.size()}"
  lines += "${ind}-- schemas:  ${schemas}"
  lines += "${ind}-- scriptDir:  ${scriptDir}"
  lines += "${ind}-- indent:  ${indent}"  
  
  scripts.each { it ->
    it = it.trim()
    if (it.length()>0) {
      lines += "${ind}@${scriptDir}/${it}"
    }
  }
  rawSql.each { it ->
    if (it.trim().length() > 0) {
      lines += ind+it
    }
  }
  lines+="${ind}exit;"
  
  return lines
  

}
def databases() {
  // read json:  
  def jsonSlurper = new JsonSlurper()
  def map = jsonSlurper.parseText('{ "name": "John Doe" }')
  return map

}
def databaseJson() {
  File jsonFile = new File('/var/lib/jenkins/userContent/config/databases.json')
  if (!jsonFile.exists()) {
    throw new RuntimeException("missing jsonFile file at: ${jsonFile.absolutePath}")
  }
  return jsonFile.text

}
class TnsEntry implements Serializable {
  String schema,host
  int port
  TnsEntry(String schema="unknown-schema",String host="unknown-host",int port=15521) {
    this.schema = schema
    this.host = host
    this.port = port
  }
  String toString() {
    return String.format("schema:%s, host=%s, port=%d", schema, host, port)
  }
}
def parseTnsnames(file = tnsnamesFile()) {
  // https://rubular.com/r/o9AjrFUq5q
  //println("TODO: parseTnsnames: file=${file.absolutePath}")
  //println(file.text)
  lines=[]
  tnsList=[]
  tns=null
  map=new HashMap<String, TnsEntry>()
  file.readLines().each { line->
    line=line.trim()
    if (line.length()>0 && !line.startsWith("#")) {
      lines+=line
      line=line.replaceAll(" ","")

      group = (line =~ /^(\w{1,50})=$/)
      if (group.hasGroup()&&group.size()==1) {
        tns = new TnsEntry(schema:group[0][1])
      } else {
        group = (line =~ /^.*\(HOST=(.{1,50})\)\(PORT=(\d{4,5})\)\)$/)
        if (group.hasGroup()) {
          if(group.size()>0) {
            tns.host=group[0][1]
            tns.port=Integer.parseInt(group[0][2])
            tnsList+=tns
            map.put(tns.schema,tns)
            tns=null

          }
        }
      }
    }
  }
  return map

}
def showTnsnames() {
  println(tnsnamesFile().text)
  return ['yes you are here']
}
def tnsnamesFile() {
  File tnsnamesFile = new File('/usr/lib/oracle/18.3/client64/lib/network/admin/tnsnames.ora')
  if (!tnsnamesFile.exists()) {
    throw new RuntimeException("missing tnsnames file at: ${tnsnamesFile.absolutePath}")
  }
  return tnsnamesFile
}
class SchemaEntry implements Serializable {
      String schema
      int deploy
      int sdlcDomain 
      String sdlcDomainName
      String appDomain
      String use
      String description
      String appEnv
      String password
      String db
  SchemaEntry(String schema="unknown-schema",
      int deploy=-3, 
      int sdlcDomain=-4, 
      String sdlcDomainName="unknown",
      String appDomain="unknown-appDomain",
      String use="unknown-use",
      String description="unknown-description",
      String appEnv="unknown-appEnv",
      String password="password",
      String db="unknown-db"
      ) {
        this.schema=schema
        this.deploy=deploy
        this.sdlcDomain=sdlcDomain
        this.sdlcDomainName=sdlcDomainName
        this.appDomain=appDomain
        this.use=use
        this.description=description
        this.appEnv=appEnv
        this.password=password
        this.db = db
      }
      String toString() {
        return String.format("schema=%s, use=%s, password=%s, db=%s",schema,use,password,db)
        //return String.format("schema=%s, desc=%s, use=%s, password=%s",schema,description,use,password)
      }
}
def getSql() {
  println("getSql: sqlInstance=${sqlInstance}")
  if (sqlInstance) {
    sqlInstance.close()
    sqlInstance=null
  }
  if(!sqlInstance) {
    String dbSchema = "gstest12"
    String dbServer = "testdb.ad.goldblattsystems.com"
    String dbUser = "dev_master"
    String dbPassword = "dev_master"
    String dbDriver = "oracle.jdbc.driver.OracleDriver"
    int dbPort = 15521
    String dbUrl = "jdbc:oracle:thin:@" + dbServer + ":${dbPort}:" + dbSchema
    sqlInstance = Sql.newInstance( dbUrl, dbUser, dbPassword, dbDriver )
  }
  println("RETURNING sqlInstance: ${sqlInstance}")
  return sqlInstance
}
def productionDbVersion() {
    if (productionSqlInstance) {
      productionSqlInstance.close()
      productionSqlInstance=null
    }
    String dbSchema = "gsprod12"
    String dbServer = "proddb.ad.goldblattsystems.com"
    String dbUser = "gsdemo"
    String dbPassword = "gsdemo"
    String dbDriver = "oracle.jdbc.driver.OracleDriver"
    int dbPort = 15521
    String dbUrl = "jdbc:oracle:thin:@" + dbServer + ":${dbPort}:" + dbSchema
    productionSqlInstance = Sql.newInstance( dbUrl, dbUser, dbPassword, dbDriver )
    qs = "select gs_version() DBVERSION from dual"
    dbVersion="UnknownDBVERSION"
    productionSqlInstance.eachRow(qs) { arow ->
        dbVersion = arow.DBVERSION
    }
    return dbVersion

}
def sdlcSchemaInfo() {
  println("sdlcSchemaInfo")
  // return new HashMap<String,String>()
  // return new HashMap<String,SchemaEntry>()
  def res = []
    String dbSchema = "gstest12"
    String dbServer = "testdb.ad.goldblattsystems.com"
    String dbUser = "dev_master"
    String dbPassword = "dev_master"
    String dbDriver = "oracle.jdbc.driver.OracleDriver"
    int dbPort = 15521
    String dbUrl = "jdbc:oracle:thin:@" + dbServer + ":${dbPort}:" + dbSchema
    sqlInstance = Sql.newInstance( dbUrl, dbUser, dbPassword, dbDriver )

  def qs = "select SCHEMA,DB,SDLC_DOMAIN_NAME,DEPLOY,SDLC_DOMAIN,APP_DOMAIN,USE,DESCRIPTION,APP_ENV,PASSWORD from dba_util.SDLC_DEPLOYMENT_INFO order by SCHEMA"
  //println("qs:${qs}")
  map=new HashMap<String,SchemaEntry>()
  //map=new HashMap<String,Map<String,Object>>()
  // anSql=getSql()
  println("sqlInstance=${sqlInstance}, eachRow Next")
  sql.eachRow(qs) { arow ->
    // Map<String,Object> sX = new HashMap<>();
    // s.put("schema",arow.SCHEMA)
    // s.put("deploy",arow.DEPLOY)
    // s.put("sdlcDomain",arow.SDLC_DOMAIN)
    // s.put("sdlcDomainName",arow.SDLC_DOMAIN_NAME)
    // s.put("appDomain",arow.APP_DOMAIN)
    // s.put("use",arow.USE)
    // s.put("description",arow.DESCRIPTION)
    // s.put("appEnv",arow.APP_ENV)
    // s.put("password",arow.PASSWORD)
    SchemaEntry s = new SchemaEntry(
      schema:arow.SCHEMA,
      deploy:arow.DEPLOY,
      sdlcDomain:arow.SDLC_DOMAIN,
      sdlcDomainName:arow.SDLC_DOMAIN_NAME,
      appDomain:arow.APP_DOMAIN,
      use:arow.USE,
      description:arow.DESCRIPTION,
      appEnv:arow.APP_ENV,
      password:arow.PASSWORD,
      db:arow.DB
    )
    map.put(arow.SCHEMA, s)
  }
  return map
}
def sdlcDomainNameList() {
  // // Oracle DB Settings
  // String dbSchema = "gstest12"
  // String dbServer = "testdb.ad.goldblattsystems.com"
  // String dbUser = "dev_master"
  // String dbPassword = "dev_master"
  // String dbDriver = "oracle.jdbc.driver.OracleDriver"
  // int dbPort = 15521
  def res = []
  // String dbUrl = "jdbc:oracle:thin:@" + dbServer + ":${dbPort}:" + dbSchema
  // def sql
  // sql = Sql.newInstance( dbUrl, dbUser, dbPassword, dbDriver )
  def qs = "select distinct(SDLC_DOMAIN_NAME) from dba_util.SDLC_DEPLOYMENT_INFO order by SDLC_DOMAIN_NAME"
  sql.eachRow(qs) { arow ->
    res+=arow.SDLC_DOMAIN_NAME
  }
  // /usr/lib/oracle/18.3/client64/lib/network/admin/tnsnames.ora
  return res

}
/*
@SourceURI
URI sourceUri
println("NEXT: printing sourceUri=${sourceUri}")

Path scriptLocation = Paths.get(sourceUri)
Path scriptDir = scriptLocation.getParent()
println("scriptLocation:${scriptLocation}")
println("scriptDir:${scriptDir}")
*/
//munge=evaluate(new File("${scriptDir}/semver.groovy"))
def verbose() {
    return true
}

def isSemver(semver) {
    (semver =~ /^(\d+)\.(\d+)\.(\d+)(-SNAPSHOT|\.RELEASE)$/).matches()
}

/**
* durationColon(long deltaSeconds)
*   deltaSeconds - number of seconds elapsed
*/
def durationColon(long deltaSeconds) {
  // println "\ndurationColon : ${deltaSeconds}"
  // println "(deltaSeconds%3600l) : ${(deltaSeconds%3600l)}"
  // println "(deltaSeconds%3600l)/60l : ${(deltaSeconds%3600l)/60l}"
  // println "(deltaSeconds%60l) : ${(deltaSeconds%60l)}"
  def hours=(long)(deltaSeconds/3600)
  def min=(long)((deltaSeconds%3600l)/60l)
  def sec=(deltaSeconds%60l)
  // println "min=${min}, sec=${sec}"
  String.format("%02d:%02d:%02d",hours,min,sec)
}

def semverMap(semver) {
   def map=["semver":semver]
   def matcher = semver =~ /(\d{1,3})\.(\d{1,3})\.(\d{1,4})((-)(SNAPSHOT)|(\.)(RELEASE))$/
   if (matcher.matches()) {
        map["size"]=matcher[0].size()
        //map["all"]=matcher[0].getAt(0)
        map["major"]=matcher[0].getAt(1)
        map["minor"]=matcher[0].getAt(2)
        map["patch"]=matcher[0].getAt(3)
        map["suffix"]=matcher[0].getAt(4)
        map["sep"]=matcher[0].getAt(7)?:matcher[0].getAt(5)
        map["type"]=matcher[0].getAt(8)?:matcher[0].getAt(6)
        map["version"]=matcher[0].getAt(1)+"."+matcher[0].getAt(2)+"."+matcher[0].getAt(3)
   } else {
       map['error']="invalid semver"
   }
   map
}


def welcome() {
    println("Welcome to this groovy script")
}
def retrieveNexusArtifact(group, artifact, version, pathToDest) {
    if (verbose()) {
      println("pathToDest.absolutePath=${new File(pathToDest).absolutePath}")
      println("parentFile=${new File(pathToDest).parentFile.absolutePath}")
    }
    nexusHost="nexus.goldblattsystems.com"
    group_path=group.replaceAll("\\.","/")
    type=(version.endsWith("RELEASE"))?"releases":"snapshots"
    // address="https://${nexusHost}/nexus/service/local/repositories/${type}/content/${group_path}/${artifact}/${version}/${artifact}-${version}.war"
    // http://gs-nexus/nexus/service/local/artifact/maven/content\?g\=com.gs\&a\=gs-util\&v\=LATEST\&r\=snapshots\&p\=jar
    address="https://${nexusHost}/nexus/service/local/artifact/maven/content?g=${group}&a=${artifact}&v=${version}&r=${type}&p=war"
    //address="http://gs-nexus/nexus/service/local/artifact/maven/content\?g\=${group}\&a\=${artifact}\&v\=${version}\&r\=${type}\&p\=war"
    //address="http://${nexusHost}/nexus/service/local/repositories/releases/content/${group_path}/${artifact}/${version}/${artifact}-${version}.war"
    if (verbose()) {
    print """
    retrieveNexusArtifact
      group:       ${group}
      group_path:  ${group_path}
      artifact:    ${artifact}
      version:     ${version}
      type:        ${type}
      pathToDest:  ${pathToDest}
      address:     ${address}
      parent:      ${new File(pathToDest).parentFile.exists()}
    """
    }
    downloadFile(address,pathToDest)
    outfile=new File(pathToDest)
}
public void download(def address, def pathToDest) {
  new File(pathToDest).withOutputStream { out ->
    out << new URL(address).openStream()
  }
}
def downloadFile(def address, def pathToDest) {
 pathToDest=pathToDest.trim()
 if (verbose()) {
 print """
    downloadFile ...
      address:    ${address}
      pathToDest: ${pathToDest}

 """ 
 }
 def destFile = new File(pathToDest)
 def stagingDir = destFile.parentFile.absolutePath;
 new File(stagingDir).mkdirs()
   def filename = pathToDest.tokenize('/')[-1]
   def file = new FileOutputStream("${stagingDir}/${filename}")
   def protocolUrlTokens = address.tokenize(':')
   def sourceUrlAsURI = new URI(address)
   def out = new BufferedOutputStream(file)
   out << sourceUrlAsURI.toURL().openStream()
   out.close()
}

public void tomcatStatus(def host) {
    slackTomcat("status request for ${host}")
    tomcat(host,"status")
}
public void tomcatStop(def host) {
    slackTomcat("stopping ${host}")
    tomcat(host,"stop")
    timeoutMinutes = 15
    timeStart = System.currentTimeMillis()
    app = "gs-portal"  // note: this hard-coding may be a problem when deploying AP-projects
    recheckSeconds = 20
    while(isTomcatAppRunning(host,app)) {
      stopDurationMinutes = (System.currentTimeMillis() - timeStart)/60000
      if(stopDurationMinutes>timeoutMinutes) {
        throw new RuntimeException("took too long [${duration(stopDurationMinutes)}] waiting for app ${app} to stop on host ${host}")
      }
      try {
        Thread.sleep(recheckSeconds*1000)
      } catch(Exception ee) {}
    }
    slackTomcat("took ${duration((System.currentTimeMillis() - timeStart)/60000)} to stop tomcat on host ${host} (app=${app})")

}
public void tomcatStart(def host) {
    slackTomcat("start ${host}")
    tomcat(host,"start")
}
public void tomcatRestart(def host) {
    slackTomcat("restarting ${host}")
    tomcat(host,"restart")
}
public String tomcatList(def host) {
    //slackTomcat("listing ${host}")
    //print(tomcat(host,"list"))
    url="http://tomcat:bigred@${host}:${tomcatPort}/manager/text/list"
    cmd=["curl","--silent",url]
    def sout = new StringBuilder(), serr = new StringBuilder()
    def proc = cmd.execute()
    proc.consumeProcessOutput(sout, serr)
    proc.waitFor()
    if (serr!=null && serr.length() > 0) {
      return "ERROR:"+serr
    }
    if (sout.toString()=="") {
      return "TOMCATDOWN"
    }
    return "RESULT"+sout

}
def isTomcatUp(def host) {
  return tomcatList(host)!="TOMCATDOWN"
}
def isTomcatAppRunning(def host, def app) {//"test-newdev","gs-portal") {
    try {
      appRunningInfo=tomcatList(host)
      //print("isTomcatAppRunning, appRunningInfo=${appRunningInfo}")
      if (appRunningInfo=="TOMCATDOWN") return false
    } catch(Exception e) {
      //print("ERROR in isTomcatAppRunning exception, e=${e}")
      return false;
    }

    return appRunningInfo.indexOf("${app}:running")>=0
}
def isTomcatAppDeployed(def host, def app) {
    try {
      appDeployedInfo=tomcatList(host)
      if (appDeployedInfo=="TOMCATDOWN") return false
    } catch(Exception e) {
      return false;
    }

    return appDeployedInfo.indexOf("${app}")>=0
}
public String tomcat(def host, def command) {
    valid_commands = ["start","stop","restart","status","list"]
    if (command in valid_commands) {
        if(verbose()) {
          println("Running Command: ${command} on host ${host}")
        }
        if (command=="list") {
          return tomcatList(host)
        }
        // TOMCAT_RESULT=`curl --silent http://${TOMCAT_HOST}:9111/gs-cnc/tomcat/cmd/${TOMCAT_COMMAND}`
        def urlString="http://${host}:9111/gs-cnc/tomcat/cmd/${command}"
        if(verbose()) {
          println("urlString:${urlString}")
        }
        def url = new URL(urlString)
        def queryString=""
        def connection = url.openConnection()
        connection.setRequestMethod("POST")
        connection.doOutput = true

        def writer = new OutputStreamWriter(connection.outputStream)
        writer.write(queryString)
        writer.flush()
        writer.close()
        connection.connect()

        def response = connection.content.text
        if(verbose()) {
          println("response: ${response}")
        }
        return response
    } else {
        if(verbose()) {
          println("Command ${command} is invalid, must be one of ${valid_commands}")
        }
        return "Command ${command} is invalid, must be one of ${valid_commands}"
    }
}
public void upload(host,context,pathToFile) {
    println("TODO: upload")
}
public void test() {
    println("STATUS "+"-"*30)
    tomcat("dev-master","status")
    println("STOP "+"-"*30)
    tomcat("dev-master","stop")
    println("STATUS "+"-"*30)
    tomcat("dev-master","status")
    println("START "+"-"*30)
    tomcat("dev-master","start")
    println("STATUS "+"-"*30)
    tomcat("dev-master","status")

}
public void test1() {
    testAddress="https://nexus.goldblattsystems.com/nexus/service/local/repositories/releases/content/com/gs/portal/gs-portal/7.8.0.RELEASE/gs-portal-7.8.0.RELEASE.war"
    testDest="/tmp/gs-portal.war"
    downloadFile(testAddress,testDest)
}
public void removeBigfacelessLicense(def path) {
    // https://www.javaworld.com/article/2073479/searching-jar-files-with-groovy.html
    if(verbose()) {
      println("removeBigfacelessLicense from ${path}")
    }
    jarRemoveItemWithNameContaining(path,"bigfaceless-license-report-and-pdf")
}
public List<String> jarList(def pathToJar) {
    File jar = new File(pathToJar)
    if (!jar.exists()) {
        throw new RuntimeException("No jar file located at ${jar.absolutePath}")
    }
    res = []
    new JarFile(jar).entries().each { entry->
               res += entry.name
     }
     return res
}
public void jarRemoveItemWithNameContaining(def pathToJar, def partialName) {
    jarName = new File(pathToJar).absolutePath
    matchList = jarList(jarName).findAll{ it.indexOf(partialName)>=0 }
    if(verbose()) {
      println("matchList: ${matchList}")
    }
    if (matchList.size()>0) {
        filesToDelete = matchList.join(' ')
        cmd = ["zip","-d","${jarName}"] 
        cmd+=matchList
        if(verbose()) {
          println("cmd:${cmd}")
        }
        cmd.execute()
    } else {
        println("no entry with '${partialName}' found in ${jarName}")
    }
    
}
public void postWarToTomcat(def pathToWar, def host, def context) {
    // curl --silent -F "file=@${WAR}" http://${HOST}:9111/gs-cnc/tomcat/deploy/war/${CONTEXT}
    url = "http://${host}:9111/gs-cnc/tomcat/deploy/war/${context}"
    cmd=["curl","--silent","-F","file=@${pathToWar}",url]
    if(verbose()) {

    println("""

    postWarToTomcat 
    pathToWar:  ${pathToWar}
    host:       ${host}
    context:    ${context}
    cmd:        ${cmd}

    """)
    }
    cmd.execute()

}
def slack(def channel, def username, def message, def icon_emoji) {
    // curl -X POST --data-urlencode "payload={\"channel\": \"${SLACK_CHANNEL}\", \"username\": \"${SLACK_USER}\", \"text\": \"${SLACK_MESSAGE}\", \"icon_emoji\": \"${SLACK_ICON}\"}" ${WEBHOOK_URL}
    payload="payload={\"channel\": \"${channel}\", \"username\": \"${username}\", \"text\": \"${message}\", \"icon_emoji\": \"${icon_emoji}\"}"
    webhook_url="https://hooks.slack.com/services/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"
    cmd=["curl","-X","POST","--data-urlencode",payload,webhook_url]
    cmd.execute()
}
def slackTomcat(message) {
    slack("tomcat","gs-ci",message, ":leopard:")
}
def duration(def millis) {
  seconds = (int)(millis/1000)
  minutes = (int)(seconds / 60 )
  return "${seconds} seconds [${minutes} minutes]"
}
def waitForTomcatRestart(def host, def app, def timeoutMinutes, def recheckSeconds) {
  print("waitForTomcatRestart: host=${host}, app=${app}, timeoutMinutes=${timeoutMinutes}, recheckSeconds=${recheckSeconds}")
  timeStart = System.currentTimeMillis()
  while(!isTomcatAppRunning(host,app)) {
    restartDurationMillis = (System.currentTimeMillis() - timeStart)
    if(restartDurationMillis>(timeoutMinutes*60000)) {
     throw new RuntimeException("took too ${duration(restartDurationMillis)} long waiting for app ${app} to start on host ${host}")
    }
    try {
      Thread.sleep(recheckSeconds*1000)
    } catch(Exception ee) {}
  }
}
def waitForTomcatAppRestart(def host, def app, def timeoutMinutes, def recheckSeconds) {
  print("waitForTomcatAppRestart: host=${host}, app=${app}, timeoutMinutes=${timeoutMinutes}, recheckSeconds=${recheckSeconds}")
  timeStart = System.currentTimeMillis()
  while(!isTomcatAppRunning(host,app)) {
    restartDurationMillis = (System.currentTimeMillis() - timeStart)
    if(restartDurationMillis>(timeoutMinutes*60000)) {
     throw new RuntimeException("took too ${duration(restartDurationMillis)} long waiting for app ${app} to start on host ${host}")
    }
    try {
      Thread.sleep(recheckSeconds*1000)
    } catch(Exception ee) {}
  }
}
def gitCaddyTagVersions() {
  /*
  436d3d1f6e869f65d3c89e1abea75e8ef6a60a63	refs/tags/v0.8.2
  c827a71d5ddb28969aae19f388ac4b7d737fece4	refs/tags/v0.8.2^{}
  */
  gitRepoUrl="https://github.com/mholt/caddy.git"
  cmd=["git","ls-remote","--tags",gitRepoUrl]
  out=[]
  res=cmd.execute().text.split("\n").each { line->
    tmp=line.split()[1].replace("refs/tags/","").trim()
    if (!tmp.endsWith("^{}")) {
      group = (tmp =~ /^(v\d{1,3}\.\d{1,3}\.\d{1,3}$)/)
      if (group.hasGroup()&&group.size()==1) {
        out+=group[0][0]
      }
    }
  }
  return out
}
def gitProjectBranches(project, includeStandardBranches, gitUser) {
  if (!gitUser) {
    gitUser="hudson"
  }
  gitRepoUrl="ssh://${gitUser}@gs-git/opt/git/${project}"
  cmd=["git","ls-remote", "--heads", gitRepoUrl]
  out=[]
  res=cmd.execute().text.split("\n").each { line->
    tmp=line.split()[1].replace("refs/heads/","").trim()
    if (!tmp.endsWith("^{}")) {
      if (includeStandardBranches || !(tmp.equals("release") || tmp.equals("master") || tmp.equals("newdev")) ) {
        out+=tmp
      }
    }
  }
  return out

}
def tomcatUndeploy(host, app) {
  // http://gs-multi-tomcat:7105/manager/html/undeploy?path=/gs-rest-scheduler&org.apache.catalina.filters.CSRF_NONCE=ACD349F7FC723F99B6A2C10CE246A0CF
  //CMD="curl --user ${TOMCAT_MANAGER_USERNAME}:${TOMCAT_MANAGER_PASSWORD} http://${TOMCAT_HOST}:${TOMCAT_PORT}/${TOMCAT_PATH}/undeploy?path=${APP_PATH}"
  //${CMD}
  println("NEXT IS UNDEPLOY of ${app} on ${host}")
  url="http://tomcat:bigred@${host}:${tomcatPort}/manager/text/undeploy?path=/${app}"
  // println("url: ${url}")
  cmd=["curl",url]
  cmd.execute()
}
def tomcatDeploy(host, contextPath, pathToWarFile) {
  println("NEXT IS DEPLOY to  ${host} to context ${contextPath} from file ${pathToWarFile}")
  warFile=new File(pathToWarFile)
  if (!warFile.exists()) {
    throw new RuntimeException("file ${pathToWarFile} does not exist")
  }
  url="http://tomcat:bigred@${host}:${tomcatPort}/manager/text/deploy?path=${contextPath}&update=true"
  println("url: ${url}")
  cmd=["curl","-T",pathToWarFile,url]
  cmd.execute()
}
def jobSchedulerCancelAll(host) {
  println("START:Cancelling all jobs on job scheduler")
  // http://[domain]:[port]/gs-rest-scheduler/rest/cancelAll
  url="http://${host}:${tomcatPort}/gs-rest-scheduler/rest/cancelAll"
  cmd=["curl",url]
  cmd.execute()
  println("END:Cancelling all jobs on job scheduler")
}
//jed=new Semver()
// setTomcatPort(7102)
// tomcatDeploy("gs-multi-tomcat", "/jedi-joe","/tmp/jedi-joe/target/jedi-joe.war")
//println("HERE==>${gitCaddyTagVersions()}")
//println(tomcatList("gs-prod"))
//println(isTomcatUp("test-newdev"))
// setTomcatPort(7105)
// println(tomcatList("gs-multi-tomcat"))
//tomcatUndeploy("gs-multi-tomcat","gs-rest-scheduler")
// println(isTomcatAppRunning("dev-newdev","gs-portal"))
// group, artifact, version, pathToDest
//retrieveNexusArtifact("com.gs","gs-rest-scheduler","7.9.0-SNAPSHOT","/tmp/gs-rest-scheduler.war.here")
// setTomcatPort(7105)
// println(tomcatList("gs-multi-tomcat"))
// tomcatUndeploy("gs-multi-tomcat","jedi-joe")
// Thread.sleep(4000)
// println(tomcatList("gs-multi-tomcat"))
//tomcatUndeploy("gs-multi-tomcat","gs-rest-scheduler")
// println("gs-rest-scheduler "+isTomcatAppRunning("gs-multi-tomcat","gs-rest-scheduler"))
//println(isTomcatAppRunning("test-newdev","gs-portal-patient"))
//slack("venn","herman","testing slack here","FLINTSTONE")
//slackTomcat("big testing here")
//jarRemoveItemWithNameContaining("test.jar", "doc")
//removeBigfacelessLicense("test.jar")
//test()
// println(sdlcDomainNameList())
// println(sdlcSchemaInfo())

//println(duration(63003))
//println(parseTnsnames(new File("/Users/jkroub/bin/tnsnames.ora")))
// def schemas, def scriptDir, def scripts, def rawSql, def indent
//println(createSqlScriptLines(schemas:['sch1','sch2'], scriptDir:'/some/place', scripts:['scr1','scrA','scrC'], rawSql:['select * from JOE_TABLE'], indent:3))
// List<String>schemas, String scriptDir, List<String>scripts, String rawSql, int indent
//println(createSqlScriptLines(['sch1','sch2'], '/some/place', ['scr1','scrA','scrC'], ['select * from JOE_TABLE']))
// writeLinesToFile('/tmp/wilma.sql',['one','two','three'])
def isUrlLive(def inurl) {
    // THIS NEEDS WORK
    println("isUrlLive, inurl=${inurl}")
    def url = new URL(inurl) //"https://www.google.com")
        HttpURLConnection connection = (HttpURLConnection) url.openConnection()
        connection.setRequestMethod("GET")
        // connection.setConnectTimeout(10000)
        try {
        connection.connect()
        } catch(Exception e) {
          return false
        }

        if (connection.responseCode == 200 || connection.responseCode == 201) {
            def returnMessage = connection.content

            //print out the full response
            println returnMessage
            return true
        } else {
            println "Error Connecting to " + url
            return false
        }  
}
def checkUrl(url) {
  print("isUrlLive(\""+url+"\""+"  " + isUrlLive(url))  
}
def nodeProductionDbVersion() {
  // println("-------")
  // println("System.properties['user.home']:  ${System.properties['user.home']}")
  // println("-------")
  // node ~/devops/node_scripts/gs-db-version.js --host ${host}
  homeDir=System.properties['user.home']
  println("nodeProductionDbVersion: homeDir:${homeDir}")
  cmd=["/usr/local/bin/node","${homeDir}/devops/node_scripts/gs-db-version.js","--host","gs-prod"]
  println("cmd:${cmd}")
    def sout = new StringBuilder(), serr = new StringBuilder()
    def proc = null
    try {
    proc = cmd.execute()
    proc.consumeProcessOutput(sout, serr)
    proc.waitFor()
    } catch(Exception e) {
      println("PROBLEM: ${e.message}")
    }
    if (serr!=null && serr.length() > 0) {
      return "ERROR:"+serr
    }
    if (sout.toString()=="") {
      return "TOMCATDOWN"
    }
    return sout.toString()

}
//checkUrl("https://cnn.com")
//checkUrl("https://cnnBOGUS.com")
//checkUrl("http://gs-multi-tomcat:7105/manager/html")
//checkUrl("http://dev-master-rest:7105/manager/html")
//checkUrl("http://tomcat:bigred@dev-master-rest:7105/manager/html")

//print("isUrlLive(\"http://cnn.com\""+"  " + isUrlLive("http://cnn.com"))
//print("isUrlLive(\"http://cnnBOGUS.com\""+"  " + isUrlLive("http://cnnBOGUS.com"))
// Caused: java.io.NotSerializableException: groovy.sql.Sql from productionDbVersion
// println("productionDbVersion=${productionDbVersion()}")
// println("productionDbVersion().getClass().getName()=${productionDbVersion().getClass().getName()}")
//println("nodeProductionDbVersion: ${nodeProductionDbVersion()}" )
def getProjects() {
   return [
  "gs-parent-pom",
"gs-mvn-depend-gwt",
"gs-parent-aw-pom",
"gs-data-services-pom",
"gs-util",
"gs-profiles",
"gs-util-persistence",
"gs-common-external",
"gs-dea",
"gs-firstdb",
"gs-flash",
"gs-growth-statistics",
"gs-mdht",
"gs-pdf-maker",
"gs-plot",
"gs-timeline",
"gs-epsdt",
"gs-rest-integration",
"gs-domain-services",
"gs-surescripts",
"gs-portal-web",
"gs-qmservice",
"gs-connect-util",
"gs-connect-gshapi",
"gs-direct-mail",
"gs-billing",
"gs-connect-rx",
"gs-awl-assembly",
"gs-awl-templating",
"gs-awl-theme",
"gs-awl-resources",
"gs-awl-root",
"gs-awl-model",
"gs-awl-converter",
"gs-awl-widget",
"gs-awl-appwidget",
"gs-awl-abstractview",
"gs-aw-document",
"gs-aw-cda",
"gs-aw-currentproblem",
"gs-aw-patient-dashboard",
"gs-aw-mailbox",
"gs-aw-pathistory",
"gs-aw-homepage",
"gs-aw-scheduler-maintenance",
"gs-aw-scheduler",
"gs-aw-demographics",
"gs-aw-role-preferences",
"gs-aw-allergy",
"gs-aw-keyfield",
"gs-aw-forms",
"gs-aw-pharmacy",
"gs-aw-practitioner",
"gs-aw-patient",
"gs-aw-medication",
"gs-aw-treatment",
"gs-aw-immunization",
"gs-aw-cda-maintenance",
"gs-aw-auditlog",
"gs-aw-patient-reports",
"gs-aw-user",
"gs-aw-orders",
"gs-aw-triagebuilder",
"gs-aw-pomr",
"gs-aw-person",
"gs-aw-codevalue",
"gs-aw-lab",
"gs-aw-task",
"gs-aw-site",
"gs-aw-clinic",
"gs-aw-ancillary",
"gs-aw-labordercode",
"gs-aw-clinicalrule",
"gs-aw-labwork",
"gs-aw-labreports",
"gs-aw-about",
"gs-aw-qualitymeasure",
"gs-aw-user-group",
"gs-aw-clinicaldocumentation",
"gs-aw-qrda",
"gs-aw-clinic-contact",
"gs-aw-template-maintenance",
"gs-aw-aoe-maintenance",
"gs-aw-externalclinic",
"gs-aw-polypharmacy",
"gs-portal-base",
"gs-portal",
"gs-portal-patient",
"gs-rest-message",
"gs-rest-connect",
"gs-connect",
"gs-rest-scheduler"
]
}
def getTomcatHosts() {
  [
    "test-release",
    "test-release-2",
    "test-master",
    "test-master-2",
    "test-newdev",
    "dev-release",
    "dev-master",
    "dev-newdev",
    "client-test",
    "gs-demobox",
    "sme",
    "gs-prod",
    "test-ap",
    "dev-ap"

  ]
}
//println(gitProjectBranches("gs-util",false, null))
//println("getProjects():${getProjects()}")
//println("projects:${projects}")
group = "com.gs"
artifact = "gs-rest-scheduler"
version = "7.9.0.RELEASE"
pathToDest="/tmp/gs-rest-scheduler.war"
// println("go time")
// retrieveNexusArtifact(group, artifact, version, pathToDest)
// println(getTomcatHosts())
return this