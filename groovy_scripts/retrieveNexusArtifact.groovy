import groovy.json.JsonSlurper
/**
https://help.sonatype.com/repomanager3/rest-and-integration-api/assets-api
**/

def footer() {
    return """
      Examples:

         ... --group com.gs.portal --artifact gs-portal --version 8.0.8.RELEASE --verbose 
         ... --group com.gs.portal --artifact gs-portal --version 8.0.8.RELEASE --extension war
         ... --group com.gs --artifact gs-util --version 8.0.8.RELEASE --extension jar
         ... --group com.gs --artifact gs-util --version 8.0.9-SNAPSHOT --extension jar
         ... --group com.gs --artifact gs-util --version 8.0.9-SNAPSHOT --extension jar --outfile /tmp/different_output.jar
         ... --group com.gs --artifact gs-rest-scheduler --version 8.0.9-SNAPSHOT --extension war --nexusUsername some_username --nexusPassword some_password
         ... --group com.gs --artifact gs-rest-scheduler --version 8.0.9-SNAPSHOT --extension war

      Notes:
        if you get 'Caught: java.lang.IllegalArgumentException: Text must not be null or empty' more than likely you have bad nexus credentials

    """
}
nexusUsername = "unknown-username"
nexusPassword = "unknown-password"

def nexusCredentials() {
    String filename = System.properties['user.home']+"/"+'.gs-nexus-credentials'
    File f = new File(filename)
    if (f.exists()) {
        def tempNexusCredentials=f.text
        if (tempNexusCredentials && tempNexusCredentials.split(":").length==2) {
            def pieces = tempNexusCredentials.split(":")
            nexusUsername = pieces[0].trim()
            nexusPassword = pieces[1].trim()
        }
    }
}
nexusCredentials()

cli = new CliBuilder(usage:" ${args}", footer:footer(), header:"")
cli.g(longOpt:'group',args:1, argName:'group','GAV group [e.g. com.gs.portal]')
cli.a(longOpt:'artifact', args:1, argName:'artifact','specify artifact [e.g. gs-portal]')
cli.v(longOpt:'version', args:1, argName:'sem-version','GAV version [e.g. 8.0.8.RELEASE]')
cli.x(longOpt:'extension', args:1, argName:'extension','package type [e.g. war, jar, pom - default: war]')
cli.u(longOpt:'nexusUsername', args:1, argName:'theNexusUsername','username of user with read privilege on nexus')
cli.p(longOpt:'nexusPassword', args:1, argName:'theNexusPassword','password of user with read privilege on nexus')
cli.h(longOpt:'help', 'show usage')
cli._(longOpt:'verbose', 'show more output')
cli._(longOpt:'outfile', args:1, argName:'outfile', 'path to output file [e.g. /path/to/gs-portal.war]')
// cli._(longOpt:'skip', 'will not actually run command')
options = cli.parse(args)


def help() {
    cli.usage()
}
def quit() {
    System.exit(0)
}
def abort(msg) {
    println("ABORT:${msg}")
    System.exit(132)
}
reasons=[]
if(options.help) {
    help()
    quit()
}
if (!options.group) {
    reasons+="missing group option"
} else {
    groupId=options.group
}
if (!options.artifact) {
    reasons+="missing artifact option"
} else {
    artifactId=options.artifact
}
if (!options.version) {
    reasons+="missing version option"
} else {
    version=options.version
}
if (!reasons.empty) {
    abort("missing: ${reasons}")
}
if (options.nexusUsername) {
    nexusUsername = options.nexusUsername
}
if (options.nexusPassword) {
    nexusPassword = options.nexusPassword
}
outfile=(options.outfile?options.outfile:null)
extension=(options.extension)?options.extension:"war"
verbose=(options.verbose)?true:false
skip=(!!options.skip)

def _run() {
    def gav = "${groupId}:${artifactId}:${version}"
    if (verbose) {
    println("""
        options:    ${options}
        gav:        ${gav}
        groupId:    ${groupId}
        artifactId: ${artifactId}
        version:    ${version}
        extension:  ${extension}
        outfile:    ${outfile}
        nexusUsername: ${nexusUsername}
        nexusPassword: ${nexusPassword}
    """)
    } 
    pullGav(gav,extension)
    
}

_run()


/**
curl -X GET 'https://nexus.goldblattsystems.com/nexus/service/rest/v1/assets?repository=goldblattsystems' -u nexus_jedi:hampster
**/
def credentials() {
    return ["username":nexusUsername,"password":nexusPassword]
}
def username() {
    credentials()['username']
}
def password() {
    credentials()['password']
}

def listRepo(repository, continuationToken=null) {
    def url="https://nexus.goldblattsystems.com/nexus/service/rest/v1/assets?repository=${repository}"
    if (continuationToken) {
        url+="&continuationToken=${continuationToken}"
    }

    def cmd=["curl","--silent", "-H", "accept: application/json", "-X", "GET", "-u", "${username()}:${password()}",url]
    if (verbose) {
        println("curl without --silent flag")
        cmd=["curl","-H", "accept: application/json", "-X", "GET", "-u", "${username()}:${password()}",url]
    } else {
        println("NO-TO-VERBOSE")
    }

    def resJson=cmd.execute().text  
    def jsonSlurper = new JsonSlurper()
    def result = jsonSlurper.parseText(resJson)
    result.items.each {
        println("------------------------------------------------------------------------")
        println(it)
    }
    continuationToken = result['continuationToken']
    println("continuationToken: ${continuationToken}")
    println(result.items.size())
    if (continuationToken) {
        listRepo(repository, continuationToken)
    }

}

/**
nexus user must have nx-admin
**/ 
def blobStores() {
    def url="https://nexus.goldblattsystems.com/nexus/service/rest/beta/blobstores"
    println("url: ${url}")
    // curl -X GET "https://nexus.goldblattsystems.com/nexus/service/rest/beta/blobstores" -H "accept: application/json"
    def cmd=["curl","--silent", "-H", "accept: application/json", "-X", "GET", "-u", "${username()}:${password()}",url]
    def resJson=cmd.execute().text  
    println(resJson)
    def jsonSlurper = new JsonSlurper()
    
    def result = jsonSlurper.parseText(resJson)
}

/**
https://help.sonatype.com/repomanager3/rest-and-integration-api/repositories-api
**/
def listRepositories() {
    println("listRepositories")
    def url="https://nexus.goldblattsystems.com/nexus/service/rest/v1/repositories"
    println("url: ${url}")
    def cmd=["curl","--silent", "-H", "accept: application/json", "-X", "GET", "-u", "${username()}:${password()}",url]
    def resJson=cmd.execute().text  
    println(resJson)
    def jsonSlurper = new JsonSlurper()
    
    def result = jsonSlurper.parseText(resJson)
}

def pullGavRelease(gav, extension="war") {
    def path=parseGav(gav)['groupId'].replaceAll(".","/")
    def groupId=parseGav(gav)['groupId']
    def artifactId=parseGav(gav)['artifactId']
    def version=parseGav(gav)['version']
    def outputFileName=(outfile)?outfile:"${artifactId}.${extension}"
    def artifactFullName="${artifactId}-${version}.${extension}"

    // https://nexus.goldblattsystems.com/nexus/service/rest/v1/search/assets?maven.artifactId=gs-util&maven.baseVersion=8.0.8.RELEASE&maven.extension=jar&maven.groupId=com.gs
    //def url = "https://nexus.goldblattsystems.com/nexus/repository/releases/com/gs/gs-util/8.0.8.RELEASE/gs-util-8.0.8.RELEASE.jar"
    def url = "https://nexus.goldblattsystems.com/nexus/service/rest/v1/search/assets?maven.artifactId=${artifactId}&maven.baseVersion=${version}&maven.extension=${extension}&maven.groupId=${groupId}"
    def cmd=["curl","--silent", "-X", "GET", "-u", "${username()}:${password()}","-H", "accept: application/json",url]
    if (verbose) {
        cmd=["curl","-X", "GET", "-u", "${username()}:${password()}","-H", "accept: application/json",url]
        println("""
            pullGavRelease
            gav:               ${gav}
            path:              ${path}
            username:          ----${username()}----
            artifactFullName:  ${artifactFullName}
            url:               ${url}
            cmd:               ${cmd}
        """)
    } 
    def resJson=cmd.execute().text
    def jsonSlurper = new JsonSlurper()
    def result = jsonSlurper.parseText(resJson)
    if (verbose)println("no. items: ${result['items'].size()}")
    items=result['items']
    if (!items || items.empty) {
        abort("no nexus artifact for ${gav} - ${extension}")
    }
    def found=items.find { item ->
       item.path.endsWith(artifactFullName)

    }
    if (!found || found.empty) {
        abort("no nexus artifact found for ${gav} - ${extension}")
    }
    if (verbose)println("found: ${found}")
    downloadFromNexus(found.downloadUrl, outputFileName)


}

def pullGavSnapshot(gav, extension="war") {
    // https://nexus.goldblattsystems.com/nexus/service/rest/v1/search/assets?maven.artifactId=gs-portal&maven.baseVersion=8.0.9-SNAPSHOT&maven.extension=war
    def groupId=parseGav(gav)['groupId']
    def artifactId=parseGav(gav)['artifactId']
    def version=parseGav(gav)['version']
    def outputFileName=outfile?outfile:"${artifactId}.${extension}"
    def url = "https://nexus.goldblattsystems.com/nexus/service/rest/v1/search/assets?maven.artifactId=${artifactId}&maven.baseVersion=${version}&maven.groupId=${groupId}&maven.extension=${extension}&sort=version&direction=desc"
    def cmd=["curl","--silent", "-X", "GET", "-u", "${username()}:${password()}","-H", "accept: application/json",url]
    if (verbose) {
        cmd=["curl","-X", "GET", "-u", "${username()}:${password()}","-H", "accept: application/json",url]
        println("""
            pullGavSnapshot
            username:          ----${username()}----
            gav:            ${gav}
            groupId:        ${groupId}
            artifactId:     ${artifactId}
            version:        ${version}
            extension:      ${extension}
            outputFileName: ${outputFileName}
            cmd:            ${cmd}
        """)
    } 

    def resJson=cmd.execute().text
    if(verbose)println("resJson===${resJson}")
    def jsonSlurper = new JsonSlurper()
    def result = jsonSlurper.parseText(resJson)
    items=result['items'].sort{ l,r-> 
      -l.path.compareToIgnoreCase(r.path)
    }
    if(verbose)println("found items: ${items}")
    if(verbose) {
        items.each{
            println(it)
        }    
    }

    // items.each{
    //     println(it)
    // }    
    if (items.empty) {
        throw new RuntimeException("no items found for ${gav}")
    }
    def downloadUrl=items[0].downloadUrl
    if(verbose)println("downloadUrl: ${downloadUrl}")
    downloadFromNexus(downloadUrl,outputFileName)

}

def downloadFromNexus(downloadUrl, outputFileName) {
    def cmd=["curl","--silent", "-X", "GET", "-u", "${username()}:${password()}","-o", "${outputFileName}",downloadUrl]
    def sout = new StringBuilder(), serr = new StringBuilder()
    def proc = cmd.execute()
    proc.consumeProcessOutput(sout, serr)
    proc.waitFor()
    if (serr!=null && serr.length() > 0) {
      abort("ERROR:"+serr)
    }
    if(verbose) {
        println(sout)
    }
    File file = new File(outputFileName);
    if(verbose) {
        println("SUCCESS:downloadFromNexus : ${file.exists()}:${file.absolutePath}:length=${file.length()}")
    }

}

def parseGav(gav) {
    def pieces=gav.split(":")
    if (pieces.length != 3) {
        throw new RuntimeException("bad gav '${gav}'")
    }
    return ["groupId":pieces[0], "artifactId":pieces[1], "version":pieces[2]]

}

def pullGav(gav, extension="war") {
    def pieces=gav.split(":")
    if (pieces.length != 3) {
        throw new RuntimeException("bad gav '${gav}'")
    }
    def groupId=pieces[0]
    def artifactId=pieces[1]
    def version=pieces[2]
    def isRelease=!version.contains("SNAPSHOT")
    // https://nexus.goldblattsystems.com/nexus/service/rest/v1/search/assets?maven.artifactId=gs-portal&maven.baseVersion=8.0.9-SNAPSHOT&maven.extension=war
    
    if (isRelease) {
        pullGavRelease(gav,extension)
    } else {
        pullGavSnapshot(gav,extension)
    
    }
    // url = "https://nexus.goldblattsystems.com/nexus/repository/releases/com/gs/gs-util/8.0.8.RELEASE/gs-util-8.0.8.RELEASE.jar"
    // def cmd=["curl","--silent", "-X", "GET", "-u", "${username()}:${password()}","-o","gs-util.jar",url]
    // def resJson=cmd.execute()
    // println("""

    //   gav:         ${gav}
    //   groupId:     ${groupId}
    //   artifactId:  ${artifactId}
    //   version:     ${version}
    //   isRelease:   ${isRelease}

    // """)

}




// pullGav("com.gs.portal:gs-portal:8.0.9-SNAPSHOT","war")
//listRepositories()
// blobStores()
// listRepo("releases")
