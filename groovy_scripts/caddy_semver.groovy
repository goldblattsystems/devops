class CaddySemver implements Comparable<CaddySemver> {
    String version
    int major, minor, patch
    CaddySemver(version) {
        this.version = version
        checkVersion()
        def group = (version =~ /^v(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/)
        // println("group[0][1]: ${group[0][1]}")
        // println("group[0][2]: ${group[0][2]}")
        // println("group[0][3]: ${group[0][3]}")
        major = Integer.parseInt(group[0][1])
        minor = Integer.parseInt(group[0][2])
        patch = Integer.parseInt(group[0][3])
    }
    public int compareTo(CaddySemver other) {
        int result = major.compareTo(other.major) 
        if (result==0) {
            result = minor.compareTo(other.minor)
        }
        if (result==0) {
            result = patch.compareTo(other.patch)
        }
        return result
        
    }
    public String toString() {
        return "version=${version}, major=${major}, minor=${minor}, patch=${patch}"
    }
    def checkVersion() {
        def group = (version =~ /^(v\d{1,3}\.\d{1,3}\.\d{1,3}$)/)
        if(group.hasGroup()&&group.size()==1) {
          return true  
        } else {
            throw new RuntimeException("bad version ${version}, expected format v#.#.#")
        }
    }
 }

def gitCaddyTagVersions() {
  /*
  436d3d1f6e869f65d3c89e1abea75e8ef6a60a63	refs/tags/v0.8.2
  c827a71d5ddb28969aae19f388ac4b7d737fece4	refs/tags/v0.8.2^{}
  */
  gitRepoUrl="https://github.com/mholt/caddy.git"
  cmd=["git","ls-remote","--tags",gitRepoUrl]
  out=[]
  caddySemvers = []
  res=cmd.execute().text.split("\n").each { line->
    tmp=line.split()[1].replace("refs/tags/","").trim()
    if (!tmp.endsWith("^{}")) {
      group = (tmp =~ /^(v\d{1,3}\.\d{1,3}\.\d{1,3}$)/)
      if (group.hasGroup()&&group.size()==1) {
        out+=group[0][0]
        caddySemvers+=new CaddySemver(group[0][0])
      }
    }
  }
  out=[]
  Collections.sort(caddySemvers)
  caddySemvers = caddySemvers.reverse()
  caddySemvers.each { cs ->
    out+=cs.version
  }

  return out
}
println("gitCaddyTagVersions: ${gitCaddyTagVersions()}")