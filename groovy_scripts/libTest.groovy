def mylib=evaluate(new File("./lib.groovy"))
mylib.isSemver("8.1.2-SNAPSHOT")
assert mylib.isSemver("8.1.2-SNAPSHOT")
assert mylib.isSemver("8.1.2.RELEASE")
assert ! mylib.isSemver("8.1.2.SNAPSHOT")
assert ! mylib.isSemver("8.1.2-RELEASE")
sm = mylib.semverMap("8.1.2.RELEASE")
testRelease = { -> 
    semver = "8.1.2.RELEASE"
    sm = mylib.semverMap(semver)
    assert sm
    assert semver == sm['semver']
    assert '8' == sm['major']
    assert '1' == sm['minor']
    assert '2' == sm['patch']
    assert '.' == sm['sep']
    assert '.RELEASE' == sm['suffix']
    assert 'RELEASE' == sm['type']
    assert '8.1.2' == sm['version']
}
testSnapshot = { -> 
    semver = "8.1.2-SNAPSHOT"
    sm = mylib.semverMap(semver)
    assert sm
    assert semver == sm['semver']
    assert '8' == sm['major']
    assert '1' == sm['minor']
    assert '2' == sm['patch']
    assert '-' == sm['sep']
    assert '-SNAPSHOT' == sm['suffix']
    assert 'SNAPSHOT' == sm['type']
    assert '8.1.2' == sm['version']
}
testDurationColon = { ->
  int hours=0
  int min=12
  int sec=35
  long secs = (long)((hours*60)+(min*60)+(sec))
  assert "00:12:35" == mylib.durationColon(secs)

  hours=5
  min=3
  sec=7
  secs = (long)((hours*3600)+(min*60)+(sec))
  assert "05:03:07" == mylib.durationColon(secs)

}
testRelease()
testSnapshot()
testDurationColon()

println "all pass"