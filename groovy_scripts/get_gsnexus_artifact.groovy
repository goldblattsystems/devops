/**
http://docs.groovy-lang.org/latest/html/gapi/groovy/cli/commons/CliBuilder.html

Example:
groovy groovy_scripts/get_gsnexus_artifact.groovy --group com.gs.portal --artifact gs-portal --version 7.8.1.RELEASE --verbose --skip

*/


cli = new CliBuilder(usage:" ")
cli.g(longOpt:'group',args:1, argName:'group','GAV group [e.g. com.gs.portal]')
cli.a(longOpt:'artifact', args:1, argName:'artifact','specify artifact [e.g. gs-portal]')
cli.v(longOpt:'version', args:1, argName:'sem-version','GAV version [e.g. 7.8.1.RELEASE]')
cli.h(longOpt:'help', 'show usage')
cli._(longOpt:'verbose', 'show more output')
cli._(longOpt:'outfile', args:1, argName:'outfile', 'path to output file [e.g. /path/to/gs-portal.war]')
cli._(longOpt:'skip', 'will not actually run command')
options = cli.parse(args)
def help() {
    cli.usage()
}
def quit() {
    System.exit(0)
}
def abort(msg) {
    println("ABORT:${msg}")
    System.exit(132)
}

reasons=[]
if(options.help) {
    help()
    quit()
}
if (!options.group) {
    reasons+="missing group option"
}
if (!options.artifact) {
    reasons+="missing artifact option"
}
if (!options.version) {
    reasons+="missing version option"
}
if (!reasons.empty) {
    abort("missing: ${reasons}")
}
verbose=(!!options.verbose)
skip=(!!options.skip)

nexusHost="nexus.goldblattsystems.com"
group=options.group //"com.gs.portal"
group_path=group.replaceAll("\\.","/")
artifact=options.artifact //"gs-portal"
version=options.version //"7.8.1.RELEASE"
curl_silent=(!!options.verbose)?"":"--silent"
outfile=(!!options.outfile)?options.outfile:"${artifact}.war"
def url="https://nexus.goldblattsystems.com/nexus/repository/releases/com/gs/gs-util/8.0.8.RELEASE/gs-util-8.0.8.RELEASE.jar"
// https://nexus.goldblattsystems.com/nexus/repository/releases/
// com/gs/gs-util/8.0.8.RELEASE/gs-util-8.0.8.RELEASE.jar
// cmd="curl ${curl_silent} -u \"nexus_jedi:hampster\" -X GET -o ${outfile} https://${nexusHost}/nexus/service/local/repositories/releases/content/${group_path}/${artifact}/${version}/${artifact}-${version}.war"
cmd="curl ${curl_silent} -u \"nexus_jedi:jJbGJw4a\" -X GET -o ${outfile} https://${nexusHost}/nexus/repository/releases/content/${group_path}/${artifact}/${version}/${artifact}-${version}.jar"
// cmd="curl ${curl_silent} -u \"nexus_jedi:hampster\" -X GET -o ${outfile} https://${nexusHost}/nexus/repository/releases/content/${group_path}/${artifact}/${version}/${artifact}-${version}.jar"

if(verbose) {
    println """
    nexusHost:  ${nexusHost}
    group:      ${group}
    group_path: ${group_path}
    artifact:   ${artifact}
    version:    ${version}
    outfile:    ${outfile}
    skip:       ${skip}
    cmd:        ${cmd}
    """
}
// https://nexus.goldblattsystems.com/nexus/repository/releases/
//cmd="curl -X GET -o ${artifact}.war http://${nexusHost}/nexus/service/local/repositories/releases/content/com/gs/portal/gs-portal/${version}/gs-portal-${version}.war"
if(!skip) {
    def proc = "${cmd}".execute()
// curl -X GET -u ${NEXUS_ADMIN_USERNAME}:${NEXUS_ADMIN_PASSWORD} "http://gs-nexus.gs.local/nexus/service/local/repositories/releases/content/com/gs/gs/7.0.5.RELEASE/gs-7.0.5.RELEASE.war"
// /com/gs/portal/gs-portal/7.8.1.RELEASE/gs-portal-7.8.1.RELEASE.war
// cURL uses error output stream for progress output.
    def sout = new StringBuilder(), serr = new StringBuilder()
    proc.consumeProcessOutput(sout, serr)
    //Thread.start { System.err << proc.err }
    // Wait until cURL process finished and continue with the loop.
    proc.waitFor()
    println "out> $sout err> $serr"
    println("AFER "+proc.exitValue())
    println("AFER "+proc.class.name)
} else {
    println("skipped running command")
}