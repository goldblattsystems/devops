_from gs-ci_
# gs-devops #


## Installation ##

* npm install
* node index.js

## notes ##

* after committing run:
```
update_devops_gsci

update_devops_gsci='echo '\''update devops on gs-ci'\''; ssh jenkins@gs-ci '\''cd devops; git pull; npm install; git reset --hard ;'\'
```
* server must have 'zip' installed [sudo yum --assumeyes install zip]
* [using zip to delete a file from a jar file](https://stackoverflow.com/questions/4520822/is-there-a-quick-way-to-delete-a-file-from-a-jar-war-without-having-to-extract)
* added nexus-repo

## Dependencies ##

* oracle/node-oracledb.git#v3.0.0
* [axios#^0.18.0](https://www.npmjs.com/package/axios)
* [commander#^2.19.0](https://www.npmjs.com/package/commander)


## Links ##

* [Install oracledb instructions](https://oracle.github.io/node-oracledb/INSTALL.html#overview) | [linuxinstall](https://oracle.github.io/node-oracledb/INSTALL.html#linuxinstall)

