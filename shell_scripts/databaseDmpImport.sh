#!/bin/bash
############################################################
# PURPOSE: import .dmp file to myoracle
# import_test_master & import_dev_master & import_dictionary_all 
#
# . $HOME/.util_env
#
# ${UTIL_BIN}/import_user_schemas gstest_users dev_master
############################################################
. $(dirname "${0}")/lib.sh

############################################################
# _setupDatabaseDmpImport
# ... ${HOME}/.util_env
############################################################
# echo "\${@}='${@}'"
# testparms="one,two,three"
# testparms=${testparms//,/ }
# echo "\${testparms}=${testparms}"
# read freak jip <<< "${testparms}"
# echo "freak:${freak}"
# echo "jip:${jip}"
_util_env() {
  echo "_util_env"
  export ORACLE_SID=gsorcl

  export UTIL_ROOT=${HOME}/util
  export UTIL_DUMPFILES=${UTIL_ROOT}/dump_files
  export UTIL_BIN=${UTIL_ROOT}/bin
  export UTIL_SCRIPTS=${UTIL_ROOT}/scripts
  export UTIL_LOGS=${UTIL_ROOT}/log
  export UTIL_IMP_DIR=IMP_CLONE_DIR

  export UTIL_USERS=clienttest,dev_release,dev_master,dev_newdev,gshold,surescripts,dba_util,gshold
  export UTIL_DICTIONARY=fdbprod,formulary,gsmaster,snomed,rxnorm,hcpcs,hcpcs_archive,radlex,sme,loinc

  if [[ -n "${verbose}" ]]; then
    echo "
        *** _util_env ***
        ORACLE_SID:        ${ORACLE_SID}
        UTIL_ROOT:         ${UTIL_ROOT}
        UTIL_DUMPFILES:    ${UTIL_DUMPFILES}
        UTIL_BIN:          ${UTIL_BIN}
        UTIL_SCRIPTS:      ${UTIL_SCRIPTS}
        UTIL_LOGS:         ${UTIL_LOGS}
        UTIL_IMP_DIR:      ${UTIL_IMP_DIR}
    "
  fi

}
_setupDatabaseDmpImport() {
  [[ "${#}" -eq 1 ]] || abort "_setupDatabaseDmpImport expected 1 arg, found '${#}'"
  local dmp_file="${1}"
  echo "_setupDatabaseDmpImport"
  _util_env
  _validate "${dmp_file}"
  which impdp &> /dev/null
  local status="${?}"
  if [[ "${status}" -ne 0 ]]; then
      abort "cannot find utility 'impdp' on PATH : ${status}"
  fi
}
_validate() {
  [[ "${#}" -eq 1 ]] || abort "_validate expected 1 arg, found '${#}'"
  local dmp_file="${1}"
  echo "_validate"
  [[ -d "${UTIL_DUMPFILES}" ]] || abort "missing directory UTIL_DUMPFILES=${UTIL_DUMPFILES}"
  [[ -f "${UTIL_DUMPFILES}/${dmp_file}" ]] || abort "missing dmp_file : '${UTIL_DUMPFILES}/${dmp_file}'"
}
_detectSchemas() {
  [[ "${#}" -eq 1 ]] || abort "_detectSchemas expected 1 arg, found '${#}'"
  local dmp_file="${1}"
  dmp_file=$(echo "${dmp_file}" | xargs)
  local regex="^(([0-9a-zA-Z\_\.]+)-([0-9a-zA-Z\_\.]+))-([0-9a-zA-Z\_\.\-]+)\.dmp\$"
  if [[ "${dmp_file}" =~ ${regex} ]]
  then
    local n=${#BASH_REMATCH[*]}
    # echo "n=${n}"
    # echo "\${BASH_REMATCH[1]}=${BASH_REMATCH[1]}"
    schemas=$(echo ${dmp_file} | sed "s/${BASH_REMATCH[1]}-//g")
    schemas=$(echo "${schemas%\.dmp}" | tr '-' ',')
    echo "${schemas}"
  fi
}

_createDefaultUser() {
  [[ "${#}" -eq 1 ]] || abort "_createShellUser expected 1 arg, found '${#}'"
  local schema="${1}"
  local password="$(echo "${schema}" | tr '[:upper:]' '[:lower:]')"
  echo "_createDefaultUser: user: ${schema}, password: ${password}"
  local script="
    set verify off;
    set newpage 0; 
    set echo off; 
    set heading off; 
    set feedback off;
    set trimspool on;
    SET UNDERLINE #;
    set underline off;
    set pagesize  0 embedded on;
    --SET PAGESIZE 0;
CREATE USER \"${schema}\" identified by \"${password}\" DEFAULT TABLESPACE GSDEV TEMPORARY TABLESPACE TEMP;
GRANT CONNECT to ${schema};
GRANT RESOURCE to ${schema};
GRANT CREATE SEQUENCE to ${schema};
GRANT CTXAPP to ${schema};
GRANT EXECUTE on CTX_DDL to ${schema};
GRANT GS_REST to ${schema};
GRANT DEBUG CONNECT SESSION to ${schema};
GRANT UNLIMITED TABLESPACE to ${schema};
GRANT CREATE MATERIALIZED VIEW to ${schema};
GRANT CREATE DATABASE LINK to ${schema};
GRANT CREATE VIEW to ${schema};
GRANT CREATE JOB to ${schema};
GRANT SELECT on V_\$SESSION to ${schema};
GRANT SELECT on V_\$LOCK to ${schema};
GRANT EXECUTE on UTL_HTTP to ${schema};
GRANT EXECUTE on DBMS_PIPE to ${schema};
GRANT EXECUTE on UTL_MAIL to ${schema};
GRANT READ on  DIRECTORY UTL_DIR to ${schema};
GRANT WRITE on  DIRECTORY UTL_DIR to ${schema};
GRANT EXECUTE on DBMS_CRYPTO to ${schema};
-- GRANT SELECT ANY DICTIONARY to ${schema};
quit

  "
  sqlplus -s '/ as sysdba' <<<"${script}"

}
############################################################
# _createShellUser <schema>
#  creates a user with all interesting privileges
############################################################
_createShellUser() {
  [[ "${#}" -eq 1 ]] || abort "_createShellUser expected 1 arg, found '${#}'"
  local schema="${1}"
  local sqlScriptsDir="${_progdir}/sql-scripts"
  [[ -d "${sqlScriptsDir}" ]] || abort "missing sqlScriptsDir '${sqlScriptsDir}'"
  local createUserScript="${sqlScriptsDir}/create_user_${schema}.sql"
  if [[ -f "${createUserScript}" ]]; then
    local script="$(cat ${createUserScript})"
    sqlplus -s '/ as sysdba' <<<"${script}"
  else
    _createDefaultUser "${schema}"
  fi

}
_importPreImport() {
  [[ "${#}" -eq 2 ]] || abort "_importPreImport expected 2 args, found '${#}'"
  local schema="${1}"
  local sid="${2}"
  echo "TODO: _importPreImport ${schema} ${sid}"
  local sqlScriptsDir="${_progdir}/sql-scripts"
  [[ -d "${sqlScriptsDir}" ]] || abort "missing sqlScriptsDir '${sqlScriptsDir}'"
  local importPreImportScript="${sqlScriptsDir}/import_pre_import.sql"
  [[ -f "${importPreImportScript}" ]] || abort "missing importPreImportScript '${creaimportPreImportScriptteUserScript}'"
  local script="$(cat ${importPreImportScript})"
  local script=$(cat ${importPreImportScript} | sed -e "s/&1/${schema}/g" -e "s/&2/${sid}/g")
  sqlplus -s '/ as sysdba' <<<"${script}"

}
_dropApplicationUser() {
  [[ "${#}" -eq 1 ]] || abort "_dropApplicationUser expected 1 arg, found '${#}'"
  local schema="${1}"
  local sqlScriptsDir="${_progdir}/sql-scripts"
  [[ -d "${sqlScriptsDir}" ]] || abort "missing sqlScriptsDir '${sqlScriptsDir}'"
  local dropUserScript="${sqlScriptsDir}/drop_application_user.sql"
  [[ -f "${dropUserScript}" ]] || abort "missing dropUserScript '${dropUserScript}'"
  # local script="$(echo $(<${dropUserScript}))"
  echo "_dropApplicationUser ${schema}"
  local script=$(cat ${dropUserScript} | sed -e "s/&1/${schema}/g")
  echo "script:${script}"
  sqlplus -s '/ as sysdba' <<<"${script}"
}


############################################################
# _dbUserCount <schema>
############################################################
_dbUserCount() {
  [[ "${#}" -eq 1 ]] || abort "_dbUserCount expected 1 arg, found '${#}'"
  local schema="${1}"
  local script="
  set head off
  set pagesize 0
  set feedback off

  select count(*) from dba_users where upper(username)=upper('${schema}');
  quit
  "
  sqlplus -s '/ as sysdba' <<<"${script}"
}
############################################################
# _dbUserExists <schema>
#  if $(_dbUserExists dev_master) ; then
#   ...
#  fi
############################################################
_dbUserExists() {
  [[ "${#}" -eq 1 ]] || abort "_dbUserExists expected 1 arg, found '${#}'"
  local schema="${1}"
  local count=$(_dbUserCount "${schema}")
  if [[ "${count}" -ne 0 ]]; then
    true
  else
    false
  fi

}
_importSchema() {
  [[ "${#}" -eq 2 ]] || abort "_importSchema expected 2 args, found '${#}'"
  local STARTING_TIME="${SECONDS}"
  local schema="${1}"
  local dmp_file="${2}"
  _dbUserExists "${schema}"
  if $(_dbUserExists "${schema}")
  then
    _dropApplicationUser "${schema}"
  fi
  _createShellUser "${schema}"
  _importPreImport "${schema}" "${ORACLE_SID}"
  impdp \'/ as sysdba\' logtime=all silent=banner schemas=${schema} dumpfile=${dmp_file} directory=${UTIL_IMP_DIR} table_exists_action=REPLACE logfile=${schema}_import.log REMAP_TABLESPACE=GS_DATA:GSDEV REMAP_TABLESPACE=SME:GSDICTIONARY REMAP_SCHEMA=${schema}:${schema} EXCLUDE=DB_LINK TRANSFORM=OID:n
  echo "_importSchema:${schema}:$(_elapsedTimeHHMMSS ${STARTING_TIME})"


}
_importSchemas() {
  [[ "${#}" -eq 2 ]] || abort "_importSchemas expected 2 args, found '${#}'"
  
  local schemas="$(echo "${1}" | tr '[:lower:]' '[:upper:]')"
  local dmp_file="${2}"
  echo "TODO: _importSchemas : ${schemas} from '${dmp_file}'"
  local schema_list=($(echo "${schemas}" | tr ',' ' '))
  for schema in ${schema_list[@]}
  do
    _importSchema "${schema}" "${dmp_file}"
  done
  
}
_databaseDmpImport() {
  [[ "${#}" -eq 2 ]] || abort "_databaseDmpImport expected 2 args, found '${#}'"
  local dmp_file="${1}"
  local schemas="${2}"
  _setupDatabaseDmpImport "${dmp_file}"
  if [[ "${schemas}" == "" ]]; then
    _detectSchemas ${dmp_file}
    schemas="$(_detectSchemas ${dmp_file})"
  fi
  if [[ "${schemas}" == "" ]]; then
    abort "no schemas specified (or detected from name '${dmp_file}'"
  fi
  _importSchemas "${schemas}" "${dmp_file}"
  _compileInvalidDbObjects
}
_compileInvalidDbObjects() {
  [[ -z "${verbose+x}" ]] || echo "_compileInvalidDbObjects:START"
  local STARTING_TIME="${SECONDS}"
  local script="
    set verify off;
    set newpage 0; 
    set echo off; 
    set heading off; 
    set feedback off;
    set trimspool on;
    SET UNDERLINE #;
    set underline off;
    set pagesize  0 embedded on;
    --SET PAGESIZE 0;

    EXEC UTL_RECOMP.recomp_parallel(2);
    alter user system account unlock;
    quit

  "
  sqlplus -s '/ as sysdba' <<<"${script}"
  [[ -z "${verbose+x}" ]] || echo "_compileInvalidDbObjects:END:$(_elapsedTimeHHMMSS ${STARTING_TIME})"
}
_mkImpdpDir() {
  [[ "${#}" -eq 2 ]] || abort "_mkImpdpDir expected 2 args, found '${#}'"
  local directory_name="${1}"
  local directory_path="${2}"

  local script="
    set verify off
    set newpage 0; 
    set echo off; 
    set heading off; 
    set feedback off;
    set trimspool on;
    SET UNDERLINE #;
    set underline off;
    set pagesize  0 embedded on;
    --SET PAGESIZE 0;
    drop directory ${directory_name};
    create directory ${directory_name} as '${directory_path}';
    grant read,write on directory ${directory_name} to public;
    quit

  "
  sqlplus -s '/ as sysdba' <<<"${script}"
}
_listDbParameters() {
    local script="
    set verify off
    set newpage 0; 
    set echo off; 
    set heading off; 
    set feedback off;
    set trimspool on;
    SET UNDERLINE #;
    set underline off;
    set pagesize  0 embedded on;
    --SET PAGESIZE 0;
    show parameters
    quit

  "
  sqlplus -s '/ as sysdba' <<<"${script}"

}
_listDirectories() {
  # echo exit | sqlplus '/ as sysdba' 'select * from dba_directories order by;'
  local script="
    set verify off
    set newpage 0; 
    set echo off; 
    set heading off; 
    set feedback off;
    set trimspool on;
    SET UNDERLINE #;
    set underline off;
    set pagesize  0 embedded on;
    --SET PAGESIZE 0;
    select DIRECTORY_NAME || '|' || DIRECTORY_PATH from dba_directories order by DIRECTORY_NAME;
    quit

  "
  sqlplus -s '/ as sysdba' <<<"${script}"
#   sqlplus -s '/ as sysdba' <<EOF
#   set verify off
#   set newpage 0; 
#   set echo off; 
#   set heading off; 
#   set feedback off;
#   set trimspool on;
#   SET UNDERLINE #;
#   set underline off;
#   set pagesize  0 embedded on;
#   --SET PAGESIZE 0;
#   select DIRECTORY_NAME || '|' || DIRECTORY_PATH from dba_directories order by DIRECTORY_NAME;
#   quit
# EOF
}

_usage() {
    echo "
  ${YELLOW}USAGE${_normal}
    $_progname [options] 

  ${YELLOW}OPTIONS${_normal}
    -h, --help                         show this help
    --compile-invalid-db-objects       will compile invalid database objects
    --dmp-file <path_to_dmpfile>       path to the .dmp file
    --list-directories                 lists known oracle database directories
    --list-db-parameters               lists set parameters of database
    --make-directory <name> <path>     creates a database pump directory
    --schemas <sc1,sc2,...,scn>        comma-separated list of schemas to import from dmp
    --verbose                          more output

  ${YELLOW}EXAMPLES${_normal}
    $_progname --help
    $_progname --dmp-file gsdictionary.dmp
    $_progname --dmp-file gstest_users.dmp
    $_progname --dmp-file jkroub-gsdict12-loinc.dmp
    $_progname --compile-invalid-db-objects
    $_progname --list-db-parameters
    $_progname --make-directory \"bambi\" \"/home/oracle/util/dump_files\"

  ${YELLOW}NOTE${_normal}
    when done exporting the .dmp send command to myoracle:
      ssh oracle@myoracle '. .bash_profile;databaseDmpImport.sh --dmp-file jkroub-gsdict12-loinc.dmp'
      ssh oracle@myoracle '. .bash_profile;databaseDmpImport.sh --dmp-file jkroub-gsdict12-sme.dmp'
    -- assumes your .bash_profile has 
          export PATH=\"${PATH}:${HOME}/devops/shell_scripts\"
    "
}

_showParams() {
    echo "
       *** parameters ***
       compile_invalid_db_objects:  ${compile_invalid_db_objects}
       directory_name:              ${directory_name}
       directory_path:              ${directory_path}
       dmp_file:                    ${dmp_file}
       list_directories:            ${list_directories}
       list_db_parameters:          ${list_db_parameters}
       verbose:                     ${verbose}
    "
}


while [ $# -gt 0 ]
do
    case "$1" in
        -h|--help)
            _usage
            exit 0
            ;;

        --compile-invalid-db-objects)
            compile_invalid_db_objects=true
            ;;

        --dmp-file)
            shift
            dmp_file="${1}"
            ;;

        --list-db-parameters)
            list_db_parameters=true
            ;;

        --list-directories)
            list_directories=true
            ;;

        --make-directory)
            shift
            directory_name="${1}"
            shift
            directory_path="${1}"
            ;;

        --schemas)
            shift
            schemas_csv="${1}"
            ;;

        --verbose)
            verbose=true
            ;;

        *)
            abort "Do not know how to process option '$1'"
            ;;

    esac
    shift
done
[[ -z "${verbose+x}" ]] || _showParams
[[ -z "${list_db_parameters+x}" ]] || _listDbParameters
[[ -z "${list_directories+x}" ]] || _listDirectories
[[ -z "${compile_invalid_db_objects+x}" ]] || _compileInvalidDbObjects
[[ -n "${directory_name}" && -n "${directory_path}" ]] && _mkImpdpDir "${directory_name}" "${directory_path}"
[[ -z "${dmp_file+x}" ]] && abort "missing dmp_file"
STARTING_TIME="${SECONDS}"
_databaseDmpImport "${dmp_file}" "${schemas_csv}"
echo "Total duration: $(_elapsedTimeHHMMSS ${STARTING_TIME})"
