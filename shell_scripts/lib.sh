################################################
# to use:
#     . $(dirname "${0}")/lib.sh
#     . /var/lib/jenkins/devops/shell_scripts/lib.sh
################################################
# File name
readonly _progname=$(basename $0)
# File name, without the extension
readonly _progbasename=${_progname%.*}
# File directory
readonly _progdir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
# Arguments
readonly _args="$@"
# Arguments number
readonly _argnum="$#"
SAVE_LOC="`pwd`"
STARTING_TIME=${SECONDS}

abort() {
  msg "ABORT:${BUILD_USER}: ${*} - duration:$(_elapsedTime ${STARTING_TIME}) "
  exit 32
}
timestamp() {
  _timestamp
}
_timestamp() {
  date +"%Y%m%d_%H%M_%S"
}
msg() {
  echo "$(timestamp):${*}"
}
slackError() {
  [ -z ${SAVE_LOC} ] && SAVE_LOC="`pwd`"
  [ -z ${BUILD_ERR_LOG} ] && BUILD_ERR_LOG="${SAVE_LOC}/build.err.log"
  echo "slack error: ${*}" >> ${BUILD_ERR_LOG}
  local slackMessage="--slack-message '$(timestamp): no message'"
  [ ${#} -gt 0 ] && slackMessage="--slack-message '${*}'"
  local slackIcon="--slack-icon ':warning:'"
  local slackChannelList="--slack-channel-list '#venn'"
  local slackUser="--slack-user 'gs-ci-admin'"
  CMD="/usr/local/bin/node ${JENKINS_HOME}/devops/node_scripts/slack-message.js ${slackIcon} ${slackChannel} ${slackChannelList} ${slackUser} ${slackMessage}"
  eval ${CMD}

}
slack(){
  msg "slack: ${*}, JENKINS_HOME=${JENKINS_HOME}"
  local slackMessage="--slack-message '$(timestamp): no message'"
  [ ${#} -gt 0 ] && slackMessage="--slack-message '${*}'"
  local slackIcon="--slack-icon ':bellhop_bell:'"
  local slackChannelList="--slack-channel-list '#venn'"
  local slackUser="--slack-user 'gs-ci-admin'"
  CMD="/usr/local/bin/node ${JENKINS_HOME}/devops/node_scripts/slack-message.js ${slackIcon} ${slackChannel} ${slackChannelList} ${slackUser} ${slackMessage}"
  eval ${CMD}
}
slackQrda(){
  msg "slack: ${*}, JENKINS_HOME=${JENKINS_HOME}"
  local slackMessage="--slack-message '$(timestamp): no message'"
  [ ${#} -gt 0 ] && slackMessage="--slack-message '${*}'"
  local slackIcon="--slack-icon ':bellhop_bell:'"
  local slackChannelList="--slack-channel-list '#qrda-2019'"
  local slackUser="--slack-user 'jkroub'"
  CMD="/usr/local/bin/node ${JENKINS_HOME}/devops/node_scripts/slack-message.js ${slackIcon} ${slackChannel} ${slackChannelList} ${slackUser} ${slackMessage}"
  eval ${CMD}
}
readonlys=(
  _progname
  _progbasename
  _progdir
  _args
  _argnum
)
showReadonlys() {
  echo "showReadonlys"
  for i in ${readonlys[@]}
  do
    echo "${i}=${!i}"
  done
}
_elapsedTimeOld() {
    if [ ${#} -ne 1 ]
    then
        echo "WARN: unable to _elapsedTime"
    else
        local START_TIME=${1}
        local ELAPSED_TIME=$(($SECONDS - $START_TIME))
        local MINUTES=$((${ELAPSED_TIME}/60))
        echo "${ELAPSED_TIME} seconds [${MINUTES} minutes]"
    fi

}
####################################################
#
# example:
#   STARTING_TIME=${SECONDS}
#   ...
#   echo "DURATION: $(_elapsedTime ${STARTING_TIME})"
####################################################
_elapsedTime() {
    if [ ${#} -ne 1 ]
    then
        echo "WARN: unable to _elapsedTime"
    else
        local START_TIME=${1}
        local ELAPSED_TIME=$(($SECONDS - $START_TIME))
        local MINUTES=$((${ELAPSED_TIME}/60))
        echo "${ELAPSED_TIME} seconds [${MINUTES} minutes]"
    fi

}

####################################################
#
# example:
#   STARTING_TIME=${SECONDS}
#   ...
#   echo "DURATION: $(_elapsedTimeHHMMSS ${STARTING_TIME})"
####################################################
_elapsedTimeHHMMSS() {
    if [ ${#} -ne 1 ]
    then
        echo "WARN: unable to _elapsedTime"
    else
        local START_TIME=${1}
        local delta=$(($SECONDS - $START_TIME))
        printf '%02d:%02d:%02d\n' $(($delta/3600)) $(($delta%3600/60)) $(($delta%60))
    fi

}

###################################
# retrieves version of project
#  from the pom
###################################
_pomProjectVersion() {
    # requires utility 'xmllint' to be installed already
    local pom_file=pom.xml
    [[ ! -n "$1" ]] || pom_file="$1"
    [[ -f $pom_file ]] || abort "file '$pom_file' does not exist"
    ret=$(xmllint --xpath '/*[local-name()="project"]/*[local-name()="parent"]/*[local-name()="version"]/text()' $pom_file)
        if [[ $? != 0 ]]; then
           ret=$(xmllint --xpath '/*[local-name()="project"]/*[local-name()="version"]/text()' $pom_file)
        fi
    echo $ret
}

###################################
# retrieve the current git branch
# expect:  master, newdev, release
#
# ex:
#  local somevar=$(_gitBranch)
###################################
_gitBranch() {
    echo $(git rev-parse --abbrev-ref HEAD)
    # another way:
    #echo $(git symbolic-ref HEAD | sed -e 's,.*/\(.*\),\1,')
}


_gitMergeBranch() {
    [[ -n "$1" ]] || abort "getMergeBranch requires 'branch' name parameter [release, master, newdev]"
    local remoteBranch="${1}"

    git merge --quiet --no-edit origin/${remoteBranch} > /dev/null
    local result=$?
    echo ${result}
}

###################################
# merge branches
#  assumes you are in a git directory where 
#
# inputs:
#  from_branch
#  to_branch
#
# ex:
#  _mergeDownstream release master
###################################
_mergeDownstream() {
  echo "TODO:_mergeDownstream"
  [ ${#} -ne 2 ] && abort "expected 2 args found ${#} in _mergeDownstream"
  local from_branch=${1}
  local to_branch=${2}
  local temp_branch=temp_branch
  git checkout master
  local current_branch=$(_gitBranch)
  echo "_mergeDownstream, from ${from_branch} to ${to_branch}, temp_branch=${temp_branch}"
  echo "current_branch=${current_branch}"
  local pomVersion=$(_pomProjectVersion)
  echo "TESTING:pomVersion: ${pomVersion}"
  [ "${to_branch}" != "master" ] && git checkout -b ${to_branch} origin/${to_branch}
  [ "${from_branch}" != "master" ] && git checkout -b ${from_branch} origin/${from_branch}
  git checkout ${from_branch}
  echo "Status: ${?}"
  git checkout -b ${temp_branch}
  echo "Status: ${?}"
  git checkout ${to_branch}
  echo "Status: ${?}"
  local result=$(_gitMergeBranch $(temp_branch))
  if [[ ${result} == Already* ]] || [[ ${result} == Auto* ]] ;
  then
    echo "already up to date"
  else
    git push --quiet
  fi
  echo "after commit"

}
