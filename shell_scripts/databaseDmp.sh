#!/bin/bash
############################################################
# PURPOSE: make and retrieve a database .dmp file
#
# select * from DBA_DIRECTORIES;
# select * from ALL_DIRECTORIES;
############################################################
. $(dirname "${0}")/lib.sh

_usage() {
    echo "
  ${YELLOW}USAGE${_normal}
    $_progname [options] 

  ${YELLOW}OPTIONS${_normal}
    -h, --help                           show this help
    --dbhost <hostname>                  host/server with database (e.g. ${valid_dbhosts[@]})
                                         [default dbhost: ${dbhost}]
    --dbcredentials <username/password>  a / separated string of username and password
                                         [default: '${dbcredentials}']
    --dbcredentials-file <filepath>      path to a file containint credentials
    --schemas <schema1,schema2,...>      comma-separated list (no spaces) of schemas to retrieve
                                         [default: ${schemas[@]}]
    --sid <sid>                          oracle sid (e.g. ${valid_sids[@]})
                                         [default: ${sid}]
    --template-gsdemo                    from gs-demobox
    --template-emprod                    from emprod (production)
    --template-surescripts               from surescripts
    --template-test-release              from test_release
    --template-users                     sets up users .dmp (~19G)
    --template-dictionary                sets up dictionary .dmp (proddb)
    --outfile <name_of_file>             name of output file [default is a function of sid/schemas]
    --verbose                            more output

  ${YELLOW}EXAMPLES${_normal}
    $_progname --help
    $_progname --schemas dev_master,test_master
    $_progname --sid gstest12 --schemas clienttest,dev_release,dev_master,dev_newdev,test_master,test_newdev,gshold,surescripts,dba_util  --dbcredentials \"\$(cat \${HOME}/.gs-oracle-credentials-testdb)\"
    $_progname --schemas kahuna1 --dbcredentials \"sys/mysyspassword\"
    $_progname --schemas kahuna1 --dbcredentials-file \"\${HOME}/.gs-oracle-credentials\"
    $_progname --sid gstest12 --schemas dev_release,dev_master,dev_newdev,surescripts
    $_progname --schemas kahuna1,gsdefault
    $_progname --template-users --dbcredentials \"\$(cat \${HOME}/.gs-oracle-credentials)\"
    $_progname --template-dictionary --dbcredentials \"\$(cat \${HOME}/.gs-oracle-credentials)\"
    $_progname --schemas kahuna1 
    $_progname --schemas kahuna1 --dbcredentials-file \"\${HOME}/.gs-oracle-credentials\"
    $_progname --sid gsdict12 --dbhost proddb.ad.goldblattsystems.com --schemas loinc --dbcredentials-file \"\${HOME}/.gs-oracle-credentials\"
    $_progname --sid gsdict12 --dbhost proddb.ad.goldblattsystems.com --schemas fdbprod --dbcredentials-file \"\${HOME}/.gs-oracle-credentials\"
    $_progname --sid gsdict12 --dbhost proddb.ad.goldblattsystems.com --schemas formulary --dbcredentials-file \"\${HOME}/.gs-oracle-credentials\"
    $_progname --sid gsdict12 --dbhost proddb.ad.goldblattsystems.com --schemas gsmaster --dbcredentials-file \"\${HOME}/.gs-oracle-credentials\"
    $_progname --sid gsdict12 --dbhost proddb.ad.goldblattsystems.com --schemas snomed --dbcredentials-file \"\${HOME}/.gs-oracle-credentials\"
    $_progname --sid gsdict12 --dbhost proddb.ad.goldblattsystems.com --schemas rxnorm --dbcredentials-file \"\${HOME}/.gs-oracle-credentials\"
    $_progname --sid gsdict12 --dbhost proddb.ad.goldblattsystems.com --schemas hcpcs --dbcredentials-file \"\${HOME}/.gs-oracle-credentials\"
    $_progname --sid gsdict12 --dbhost proddb.ad.goldblattsystems.com --schemas hcpcs_archive --dbcredentials-file \"\${HOME}/.gs-oracle-credentials\"
    $_progname --sid gsdict12 --dbhost proddb.ad.goldblattsystems.com --schemas radlex --dbcredentials-file \"\${HOME}/.gs-oracle-credentials\"
    $_progname --sid gsdict12 --dbhost proddb.ad.goldblattsystems.com --schemas sme --dbcredentials-file \"\${HOME}/.gs-oracle-credentials\"
    $_progname --sid gsdict12 --dbhost proddb.ad.goldblattsystems.com --schemas cdc_vaccine --dbcredentials-file \"\${HOME}/.gs-oracle-credentials\"
    $_progname --sid gstest12 --dbhost testdb.ad.goldblattsystems.com --schemas  clienttest --dbcredentials-file \"\${HOME}/.gs-oracle-credentials\"
    $_progname --template-gsdemo --dbcredentials-file \"\${HOME}/.gs-oracle-credentials\"
    $_progname --template-test-release --dbcredentials-file \"\${HOME}/.gs-oracle-credentials\"
    $_progname --template-emprod --dbcredentials-file \"\${HOME}/.gs-oracle-credentials\"
    $_progname --template-surescripts --dbcredentials-file \"\${HOME}/.gs-oracle-credentials\"

  ${YELLOW}NOTE${_normal}
    when done exporting the .dmp send command to myoracle:
      ssh oracle@myoracle '. .bash_profile;databaseDmpImport.sh --dmp-file jkroub-gsdict12-loinc.dmp'
      ssh oracle@myoracle '. .bash_profile;databaseDmpImport.sh --dmp-file jkroub-gsdict12-sme.dmp'
      ssh oracle@myoracle '. .bash_profile;databaseDmpImport.sh --dmp-file jkroub-gsprod12-emprod.dmp'
    -- assumes your .bash_profile has 
          export PATH=\"\${PATH}:\${HOME}/devops/shell_scripts\"

    "

    #  clienttest dev_release dev_master dev_newdev test_master test_newdev gshold surescripts dba_util)
    # fdbprod formulary gsmaster snomed rxnorm hcpcs hcpcs_archive radlex sme loinc cdc_vaccine
}
tmp_prefix="/tmp/"
valid_dbhosts=(testdb.ad.goldblattsystems.com proddb.ad.goldblattsystems.com)
valid_sids=(gsbkup12 gstest12 gsdist12 gsprod12 gsap12 hldb12)
dumpFilesLocation="${HOME}/shared/dump_files"
script_timestamp=$(_timestamp)
schemas=(kahuna1)
sid="gsbkup12"
dbhost=testdb.ad.goldblattsystems.com
dbcredentials=""
_setup() {
  local workarea="workarea_${script_timestamp}"
  local workareaLocation="/tmp/${workarea}"
  if ! $(mkdir -p "${workareaLocation}"); then
    abort "PROBLEM making workarea: ${workarea}"
  fi
  echo "${workareaLocation}"
  
}
_tearDown() {
  rm -rf "${workareaLocation}"
}
_createTmpDatabaseDmpScript() {
  [[ "${#}" -eq 5 ]] || abort "_createTmpDatabaseDmpScript expected 5 args, found '${#}'"
  local script="${1}"
  local sid="${2}"
  local schemas="${3}"
  local outfile="${4}"
  local dbcredentials="${5}"
  if [[ "${verbose}" != "" ]]; then
    echo "
  
     **** _createTmpDatabaseDmpScript ****
     pwd:           $(pwd)
     script:        ${script}
     sid:           ${sid}
     schemas:       ${schemas}
     outfile:       ${outfile}
     dbcredentials: ${dbcredentials}
    "
  fi

  local expdp_exe="\${ORACLE_HOME}/bin/expdp}"
  local expdp_directory=EXPDP_DIR
  local oracle_home="\$(dbhome)"
  local proddb_dict_dmp_directory="/data/expdp/gsdict12"
  local testdb_dmp_directory="/data/expdp"
  local dmp_directory="${testdb_dmp_directory}"
  if [[ "${dbhost}" == "proddb.ad.goldblattsystems.com" ]]; then
    # expdp_directory=expdp_dict
    # [[ "${schemas[@]}" == "emprod" ]] && expdp_directory=expdp_emprod
    # [[ "${schemas[@]}" == "test_release" ]] && expdp_directory=expdp_emprod
    oracle_home="/app/oracle/product/12.2.0/dbhome_1"
    # dmp_directory="${dmp_directory}/${sid}"
  fi
  if [[ "${verbose}" != "" ]]; then
    echo "
  
     **** _createTmpDatabaseDmpScript ****
     pwd:               $(pwd)
     script:            ${script}
     sid:               ${sid}
     schemas:           ${schemas}
     outfile:           ${outfile}
     dbcredentials:     ${dbcredentials}
     expdp_directory:   ${expdp_directory}
    "
  fi
  echo "
#!/bin/bash
source /home/oracle/.bash_profile
echo \"just sourced \${HOME}/.bash_profile\"
# . \${HOME}/.bash_profile
export PATH=\$PATH:/usr/local/bin
[[ \"\${USER}\" == \"oracle\" ]] || { echo \"must be user 'oracle', but you are '\${USER}'\"; exit 23; }
echo 'generating .dmp for sid: ${sid} called ${outfile}'  
export ORACLE_SID=${sid}
export ORACLE_HOME=${oracle_home}
echo \"
  whoami:          \$(whoami)
  ORACLE_SID:      \${ORACLE_SID}
  ORACLE_HOME:     \${ORACLE_HOME}
  dbhome:          \$(dbhome)
  dbhost:          ${dbhost}
  date:            \$(date)
  outfile:         ${outfile}
  schemas:         ${schemas}
  script:          ${script}
  \$ORACLE_HOME/bin/expdp:  \$(ls -l \$ORACLE_HOME/bin/expdp)
  expdp_directory: ${expdp_directory}
  dbcredentials:   ${dbcredentials}
  dmp_directory:   ${dmp_directory}
  see:             https://community.oracle.com/thread/3948515
\"
# the following creates a file in /data/expdp directory named ${outfile} (and ${outfile}.log)
\$ORACLE_HOME/bin/expdp \\'${dbcredentials} as sysdba\\' directory=${expdp_directory} dumpfile=${outfile} schemas=${schemas} logfile=${outfile}.log
#\$ORACLE_HOME/bin/expdp \\'${dbcredentials} as sysdba\\' directory=EXPDP_DIR dumpfile=${outfile} schemas=${schemas} logfile=${outfile}.log
status=\"\${?}\"
if [[ \"\${status}\" -ne 0 ]]; then
   echo \"PROBLEM status [\${status}] during dmp creation \"
   exit 73
fi
ls -trl ${dmp_directory}
echo \"----------------------------------------\"
ls -trl ${dmp_directory}/${outfile}
echo \"sum ${dmp_directory}/${outfile}: \$(sum ${dmp_directory}/${outfile})\"
echo \"----------------------------------------\"
ls -trl ${dmp_directory}/${outfile}.log
echo \"----------------------------------------\"
echo \"finished: \$(date)\"

  " > "${script}"
  
}
_sendTmpDatabaseDmpScriptToServer() {
  [[ "${#}" -eq 2 ]] || abort "_sendTmpDatabaseDmpScriptToServer expected 2 args, found '${#}'"
  local script="${1}"
  local dbhost="${2}"
  [[ -f "${script}" ]] || abort "missing script file '${script}'"
  scp "${script}" "oracle@${dbhost}:${tmp_prefix}${script}" >/dev/null
  local status="${?}"
  if [[ "${status}" -ne 0 ]] ; then
    abort "_sendTmpDatabaseDmpScriptToServer with scp of ${script}"
  else
    [[ -z "${verbose+x}" ]] || echo "_sendTmpDatabaseDmpScriptToServer, status='${status}'"
  fi
  # [[ -z "${verbose+x}" ]] || ssh oracle@${dbhost} "ls -tlr ${tmp_prefix}${script}"
  [[ -z "${verbose+x}" ]] || ssh oracle@${dbhost} "cat ${tmp_prefix}${script}"
  [[ -z "${verbose+x}" ]] || echo "END:_sendTmpDatabaseDmpScriptToServer:script=${script}"

}
_executeTmpDatabaseDmpScriptToServer() {
  [[ "${#}" -eq 2 ]] || abort "_executeTmpDatabaseDmpScriptToServer expected 2 args, found '${#}'"
  local script="${1}"
  local dbhost="${2}"
  [[ -z "${verbose+x}" ]] || echo "START:_executeTmpDatabaseDmpScriptToServer:script=${script}"
  ssh oracle@${dbhost} "cd ${tmp_prefix}; source ./${script}"
  local status="$?"
  if [[ "${status}" -ne 0 ]]; then
    abort "[${status}] there was a problem while executing remote script '${script}'"
  fi
  [[ -z "${verbose+x}" ]] || echo "END:_executeTmpDatabaseDmpScriptToServer:script=${script}"

}
_retrieveDatabaseDmpFromServer() {
  [[ "${#}" -eq 3 ]] || abort "_retrieveDatabaseDmpFromServer expected 3 args, found '${#}'"
  local fileToRetrieve="${1}"
  local dumpFilesLocation="${2}"
  local dbhost="${3}"
  local destFile="${dumpFilesLocation}/$(basename ${fileToRetrieve})"
  [[ -f "${destFile}" ]] && rm "${destFile}"
  scp "oracle@${dbhost}:${fileToRetrieve}" "${dumpFilesLocation}/."
  [[ -z "${verbose+x}" ]] || echo "sum ${destFile}:  $(sum ${destFile})"
}
_cleanupDatabaseDmpOnServer() {
  [[ "${#}" -eq 4 ]] || abort "_cleanupDatabaseDmpOnServer expected 4 args, found '${#}'"
  local script="${1}"
  local outfile="${2}"
  local dbhost="${3}"
  local sid="${4}"
  local dmp_directory="/data/expdp"
  # [[ "${dbhost}" == "proddb.ad.goldblattsystems.com" ]] && dmp_directory="${dmp_directory}/${sid}"
  if [[ -n "${verbose}" ]]; then
  echo "
    *** _cleanupDatabaseDmpOnServer
    script:         ${script}
    outfile:        ${outfile}
    dbhost:         ${dbhost} 
    sid:            ${sid}
    dmp_directory:  ${dmp_directory}
  "
  fi
  # ssh oracle@${dbhost} "cd ${tmp_prefix}; rm ${script}; cd \"${dmp_directory}\"; find . -name \"${outfile}\" -exec rm  {} \\; find . -name \"${outfile}.log\" -exec rm  {} \\;"
  ssh oracle@${dbhost} "cd ${tmp_prefix}; rm ${script}; cd \"${dmp_directory}\"; rm \"${outfile}\" \"${outfile}.log\"; "
}
###########################################################################
# _performDatabaseDmp
# create tmp script
# send script to server (scp)
# execute tmp script (ssh)
# retrieve new artifact
###########################################################################
_performDatabaseDmp() {
  [[ "${#}" -eq 5 ]] || abort "_performDatabaseDmp expected 5 args, found '${#}'"
  local dbhost="${1}"
  local sid="${2}"
  local schemas="${3}"
  local outfile="${4}"
  local dbcredentials="${5}"
  local starting_time="${SECONDS}"
  local scriptName="${_progname}_tmpscript_${script_timestamp}"
  local workareaLocation=$(_setup)
  if [[ "${verbose}" != "" ]] ; then
    echo "
        **** _performDatabaseDmp ****
        scriptName:         ${scriptName}
        workareaLocation:   ${workareaLocation}
        dbhost:             ${dbhost}
        dbcredentials:      ${dbcredentials}
        sid:                ${sid}
        schemas:            ${schemas}
        which expdp:        $(which expdp)
        outfile:            ${outfile}
    "
  fi
  cd "${workareaLocation}"
  echo "Now in $(pwd), workareaLocation=${workareaLocation}"
  _createTmpDatabaseDmpScript "${scriptName}" "${sid}" "${schemas}" "${outfile}" "${dbcredentials}"
  _sendTmpDatabaseDmpScriptToServer "${scriptName}" "${dbhost}"
  _executeTmpDatabaseDmpScriptToServer "${scriptName}" "${dbhost}"
  local testdb_dmp_directory="/data/expdp"
  local dmp_directory="${testdb_dmp_directory}"
  # if [[ "${dbhost}" == "proddb.ad.goldblattsystems.com" ]]; then
    # dmp_directory="${dmp_directory}/${sid}"
  # fi
  [[ -z "${verbose+x}" ]] || echo "FILE_TO_RETRIEVE: ${FILE_TO_RETRIEVE}"
  FILE_TO_RETRIEVE="${dmp_directory}/${outfile}"
  _retrieveDatabaseDmpFromServer "${FILE_TO_RETRIEVE}" "${dumpFilesLocation}" "${dbhost}"
  _cleanupDatabaseDmpOnServer "${scriptName}" "${outfile}" "${dbhost}" "${sid}"
  _tearDown
  echo "Total duration: $(_elapsedTime ${starting_time})"
}


_showParams() {
    echo "
       *** parameters ***
       dbhost:           ${dbhost}
       no. schemas:      ${#schemas[@]}
       schemas:          ${schemas[@]}
       sid:              ${sid}
       dbcredentials:    ${dbcredentials}
       verbose:          ${verbose}
    "
}

_loadCredentials() {
  [[ "${#}" -eq 1 ]] || abort "_loadCredentials expected 1 arg, found '${#}'"
  local dbcredentials_file="${1}"
  [[ -f "${dbcredentials_file}" ]] || abort "missing db credentials file '${dbcredentials_file}'"
  cat "${dbcredentials_file}"
}

while [ $# -gt 0 ]
do
    case "$1" in
        -h|--help)
            _usage
            exit 0
            ;;

        --dbcredentials)
            shift
            dbcredentials="${1}"
            ;;

        --dbcredentials-file)
            shift
            [[ -f "${1}" ]] || abort "missing db credentials file '${1}'"
            dbcredentials=$(_loadCredentials "${1}")
            ;;

        --dbhost)
            shift
            dbhost="${1}"
            ;;

        --schemas)
            shift
            schemas=($(echo "${1}" | tr ',' ' '))
            ;;

        --sid)
            shift
            sid="${1}"
            ;;

        --template-gsdemo)
            dbhost=proddb.ad.goldblattsystems.com
            sid=gsprod12
            schemas=(gsdemo)
            ;;

        --template-test-release)
            dbhost=proddb.ad.goldblattsystems.com
            sid=gsprod12
            schemas=(test_release)
            ;;

        --template-emprod)
            dbhost=proddb.ad.goldblattsystems.com
            sid=gsprod12
            schemas=(emprod)
            ;;

        --template-surescripts)
            dbhost=proddb.ad.goldblattsystems.com
            sid=gsprod12
            schemas=(surescripts)
            ;;

        --template-dictionary)
            dbhost=proddb.ad.goldblattsystems.com
            sid=gsdict12
            schemas=(fdbprod formulary gsmaster snomed rxnorm hcpcs hcpcs_archive radlex sme loinc cdc_vaccine)
            ;;

        --template-users)
            dbhost=testdb.ad.goldblattsystems.com
            sid=gstest12
            schemas=(clienttest dev_release dev_master dev_newdev test_master test_newdev gshold surescripts dba_util)
            schemas=(dev_release dev_master)
            ;;

        --verbose)
            verbose=true
            ;;

        *)
            abort "Do not know how to process option '$1'"
            ;;

    esac
    shift
done
######################################################################
# _validateDbhost <dbhost> 
######################################################################
_validateDbhost() {
  [[ "${#}" -eq 1 ]] || abort "_validateDbhost expected 1 arg, found '${#}'"
  local dbhost="${1}"
  case "${dbhost}" in
     testdb.ad.goldblattsystems.com | proddb.ad.goldblattsystems.com)
        ;;
     *)
        abort "_validateDbhost:invalid/unknown dbhost: ${dbhost}"
        ;;
  esac
  
}
######################################################################
# _validateSid <dbhost> <sid>
######################################################################
_validateSid() {
  [[ "${#}" -eq 2 ]] || abort "_validateSid expected 2 args, found '${#}'"
  local dbhost="${1}"
  local sid="${2}"

  case "${dbhost}" in
    testdb.ad.goldblattsystems.com)
          case "${sid}" in
            gstest12|gsap12|gsbkup12|hldb12)
              ;;
            
            *)
              abort "sid '${sid}' not valid for dbhost '${dbhost}'"
              ;;

          esac
      ;;
    proddb.ad.goldblattsystems.com)
          case "${sid}" in
            gsdict12|gsprod12|hldb12)
              ;;
            
            *)
              abort "sid '${sid}' not valid for dbhost '${dbhost}'"
              ;;

          esac
      ;;
    *)
        abort "_validateSid:invalid/unknown dbhost: ${dbhost}"
        ;;
  esac

}
######################################################################
# _isSchemaInSid <schema> <sid>
######################################################################
_isSchemaInSid() {
  [[ "${#}" -eq 2 ]] || abort "_isSchemaInSid expected 2 args, found '${#}'"
  local schema="${1}"
  local sid="${2}"
  case "${sid}" in
    gstest12)
          case "${schema}" in
            gshold|dba_util|clienttest|dev_release|dev_master|dev_newdev|surescripts|test_master|test_newdev)
              return 0
              ;;
            *)
              return 101
              ;;
          esac
      ;;

    gsdict12)
          case "${schema}" in
            cms_stage|fdb_stage|fdbprod|sme|snomed|formulary|gsmaster|cdc_vaccine|loinc|radlex|rxnorm|hcpcs|hcpcs_archive)
              return 0
              ;;
            *)
              return 102
              ;;
          esac 
      ;;

    gsbkup12)
          case "${schema}" in
            gsdefault|kahuna1|kahuna2|kahuna3|release_integration|sme_test)
              return 0
              ;;
            *)
              return 103
              ;;
          esac 
      ;;

    gsprod12)
          case "${schema}" in
            gsdemo|emprod|test_release|surescripts)
              return 0
              ;;
            *)
              return 104
              ;;
          esac 
      ;;

    gsap12)
          case "${schema}" in
            ap_dev_master|ap_test_master)
              return 0
              ;;
            *)
              return 105
              ;;
          esac 
      ;;

    hldb12)
          case "${schema}" in
            hluser)
              return 0
              ;;
            *)
              return 106
              ;;
          esac 
      ;;

    *)
      return 107
      ;;
  esac
}
######################################################################
# _validateSchemasAgainstSid <schema1,schema2,...> <sid>
######################################################################
_validateSchemasAgainstSid() {
  [[ "${#}" -eq 2 ]] || abort "_validateSchemasAgainstSid expected 2 args, found '${#}'"
  local schemas=($(echo "${1}"|tr ',' ' '))
  local sid="${2}"
  for schema in "${schemas[@]}"
  do
    _isSchemaInSid ${schema} ${sid}
    local status="$?"
    if [[ "${status}" -ne 0 ]]
    then
      abort "schema '${schema}' is not available to sid '${sid}'"
    fi
  done

}
[[ -z "${verbose+x}" ]] || _showParams
[[ -z "${sid+x}" ]] && abort "missing sid"
[[ -z "${dbhost+x}" ]] && abort "missing dbhost"
[[ -z "${schemas+x}" || "${#schemas[@]}" -eq 0 ]] && abort "missing schemas"
_validateDbhost "${dbhost}"
_validateSid "${dbhost}" "${sid}"
_validateSchemasAgainstSid "$(echo "${schemas[@]}" | tr ' ' ',')" "${sid}"
[[ -z "${outfile+x}" ]] && outfile="$(echo "${USER}-${sid}-$(echo "${schemas[@]}" | tr ' ' '-' )").dmp";
_performDatabaseDmp "${dbhost}" "${sid}" "$(echo ${schemas[@]} | tr ' ' ',')" "${outfile}" "${dbcredentials}"
