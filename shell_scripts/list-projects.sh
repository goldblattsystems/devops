#!/bin/bash
. $(dirname "${0}")/lib.sh
branch="master"
jenkins_user="$(whoami)"
_listProjectsUsage() {
echo "
  Purpose:
    list all the projects for a branch

  Usage:
    $_progname <options>

  Options:
    -h, --help, ?                  show this help
    --branch <branch>              branch (release,master,newdev) [master]
    --jenkins-user <jenkins-user>  login user for jenkins server [\${USER}]
    --verbose                      more verbose

  Examples:
    $_progname --help
    $_progname --jenkins-user jenkins --branch release
    $_progname --jenkins-user jkroub --branch release
    $_progname --jenkins-user jkroub --branch release --verbose
    $_progname --branch release
    $_progname --branch master
    $_progname --branch newdev

"
    
}
_showParams() {
    echo "
      **** parameters ****
      branch:        ${branch}
      jenkins_user:  ${jenkins_user}
      verbose:       ${verbose}
    "
}
_listProjects() {
    ssh ${jenkins_user}@jenkins "export branch=${branch} ; grep "build" /var/lib/jenkins/jobs/gs-ALL-\${branch}-nexus/config.xml|sed -e s/build//g -e s/\&apos\;//g -e s/},//g -e s/EEE//g  -e s/://g -e s/{//g -e s/}//g -e s/a[0-9][0-9]//g -e s/a[0-9]//g -e s/awl[0-9]//g -e s/-master-nexus//g -e s/-newdev-nexus//g -e s/-release-nexus//g -e s/\\t//g -e s/\ //g"
}
while [ $# -gt 0 ]
do
    case "$1" in
        -h|--help)
            _listProjectsUsage
            exit 0
            ;;
        --branch)
            shift
            branch=("$1")
            ;;
        --jenkins-user)
            shift
            jenkins_user=("$1")
            ;;
        --verbose)
            verbose=true
            ;;
        *)
            abort "Do not know how to process option '$1'"
            ;;
    esac
    shift
done

[[ -z ${verbose} ]] || _showParams
#_listProjectsUsage
_listProjects