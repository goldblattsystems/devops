#!/bin/bash
############################################################
# PURPOSE: template for new scripts
# hold some usage entries
############################################################
. $(dirname "${0}")/lib.sh

_usage() {
    echo "
  ${YELLOW}USAGE${_normal}
    $_progname [options] 

  ${YELLOW}OPTIONS${_normal}
    -h, --help                         show this help
    --verbose                          more output

  ${YELLOW}EXAMPLES${_normal}
    $_progname --help

    "
}

_showParams() {
    echo "
       *** parameters ***
       verbose:  ${verbose}
    "
}


while [ $# -gt 0 ]
do
    case "$1" in
        -h|--help)
            _usage
            exit 0
            ;;

        --verbose)
            verbose=true
            ;;

        *)
            abort "Do not know how to process option '$1'"
            ;;

    esac
    shift
done
[[ -z "${verbose+x}" ]] || _showParams
_usage