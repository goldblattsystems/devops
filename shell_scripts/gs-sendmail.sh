#!/bin/bash
. $(dirname "${0}")/lib.sh
_usage() {
   cat<<EOF
  Usage:
    $_progname <options>

  Options:
    -h, --help, ?              show this help
    --from <from-address>      email address from [required]
    --to <recipients-csv>      list of recipients [required]
    --subject <subject>        subject of email [required]
    --body <body>              body of email [optional]
    --body-is-html             if present, treats body as html [optional]

  Examples:
    $_progname --to javapda@gmail.com --from "jkroub@goldblattsystems.com" --subject "from macbook pro time" --body "love it"

EOF
}
_to_line() {
  local dests=()
  local to_line=""
  for dest in ${email_to[@]}
  do
     [ "${to_line}" != "" ] && to_line+=", "
     to_line+="${dest}"
  done
  echo ${to_line}
}
_contentType() {
  local contentType="text/plain"
  if [ "${email_body_is_html}" != "true" ]
  then
    contentType="text/html"
  fi
  echo "${contentType}"
}
_content() {
  echo "${email_body}"
  
}
_send() {

sendmail ${email_to} <<EOF
From: ${email_from}
To: $(_to_line)
Subject: ${email_subject}
Content-Type: $(_contentType)
MIME-Version: 1.0

$(_content)
EOF
STATUS=$?
if [ ${STATUS} -ne 0 ]; then
   abort "Problem sending email, to=${to}, from=${from}, STATUS=${STATUS}"
fi

}

while [ $# -gt 0 ]
do
    case "$1" in
        -h|--help|?|-?)
            _usage
            exit 0
            ;;
        --body)
            shift
            email_body=("$1")
            ;;
        --body-is-html)
            email_body_is_html=true
            ;;
        --from)
            shift
            email_from=("$1")
            ;;
        --to)
            shift
            email_to=("$1")
            ;;
        --subject)
            shift
            email_subject="${1}"
            ;;
        *)
            abort "Do not know how to process option '$1'"
            ;;
    esac
    shift
done
params=(email_to email_from email_subject)
missing=()
for p in ${params[@]}
do
  [ "${!p}" == "" ]  && missing+=(${p})
  msg "${p}=${!p}"
done
[ ${#missing[@]} -ne 0 ] && abort "${#missing[@]} parameters missing - '${missing[@]}'"

[ "`which sendmail`" == "" ] && abort "cannot find 'sendmail' on path"
#_usage
_send