DECLARE
  V_PATIENT_ID NUMBER;
BEGIN
  V_PATIENT_ID := -1;
    dbms_output.put_line('Remove meds for patient id ' || v_patient_id);
    DELETE FROM gst_med_order_item
    WHERE
        guid IN (
            SELECT
                guid
            FROM
                gst_order_item
            WHERE
                master_order_guid IN (
                    SELECT
                        guid
                    FROM
                        gst_order
                    WHERE
                        patient_id = v_patient_id
                        AND order_type = 'MED_SURESCRIPTS'
                )
        );

    DELETE FROM gst_order_item_detail
    WHERE
        order_item_guid IN (
            SELECT
                guid
            FROM
                gst_order_item
            WHERE
                master_order_guid IN (
                    SELECT
                        guid
                    FROM
                        gst_order
                    WHERE
                        patient_id = v_patient_id
                        AND order_type = 'MED_SURESCRIPTS'
                )
        );

    DELETE FROM gst_order_item
    WHERE
        master_order_guid IN (
            SELECT
                guid
            FROM
                gst_order
            WHERE
                patient_id = v_patient_id
                AND order_type = 'MED_SURESCRIPTS'
        );

    DELETE order_reason_list
    WHERE
        order_guid IN (
            SELECT
                guid
            FROM
                gst_order
            WHERE
                patient_id = v_patient_id
                AND order_type = 'MED_SURESCRIPTS'
        );

    DELETE FROM gst_order_action
    WHERE
        master_order_guid IN (
            SELECT
                guid
            FROM
                gst_order
            WHERE
                patient_id = v_patient_id
                AND order_type = 'MED_SURESCRIPTS'
        );

    DELETE FROM gst_order
    WHERE
        patient_id = v_patient_id
        AND order_type = 'MED_SURESCRIPTS'
        ;

    DELETE FROM gst_result_value
    WHERE
        med_event IN (
            SELECT
                id
            FROM
                gst_clinical_event
            WHERE
                event_type = 'MED'
                AND patient = v_patient_id
                AND med_context = 'SS_INTEGRATION'
        );

    DELETE FROM gst_clinical_event
    WHERE
        patient = v_patient_id
        AND event_type = 'MED'
        AND med_context = 'SS_INTEGRATION'
        ;
END;
