set verify off
--set echo off
--set feedback off

prompt
prompt   Added Account unlock as bandaid for SYSTEM account locking
prompt

alter user system account unlock;

prompt 
prompt executing:  exec DBMS_NETWORK_ACL_ADMIN.add_privilege ('gs_rest.xml',upper('&1'), TRUE,'connect');
prompt

exec DBMS_NETWORK_ACL_ADMIN.add_privilege ('gs_rest.xml',upper('&1'), TRUE,'connect');

prompt
prompt  connect as the target user
prompt 

alter user &1 identified by &1;

connect &1/&1

prompt
prompt ** Create standard database links shunted back to &2
prompt

create database link firstdb connect to fdbprod identified by fdbprod using '&2';
create database link radlex connect to radlex  identified by radlex using '&2';
create database link snomed connect to snomed identified by snomed using '&2';
create database link hcpcs connect to hcpcs identified by hcpcs using '&2';
create database link loinc connect to loinc identified by loinc using '&2';
create database link gsmaster connect to gsmaster identified by gsmaster using '&2';
create database link sme connect to gsmaster identified by gsmaster using '&2';
-- create database link gsdict connect to system identified by B1gr3d4gs using '&2';
-- create database link gstest connect to system identified by B1gr3d4gs using '&2';
-- create database link gsap connect to system identified by B1gr3d4gs using '&2';
-- create database link gsprod connect to system identified by B1gr3d4gs using '&2';
-- create database link gsbkup connect to system identified by B1gr3d4gs using '&2';

