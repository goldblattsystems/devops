set verify off
set echo on

CREATE OR REPLACE PROCEDURE kill_user (p_user IN VARCHAR2)
IS
    v_str     VARCHAR2 (100);
    v_count   INTEGER := 0;

    CURSOR scn_user
    IS
        SELECT sid, serial#
          FROM v$session
         WHERE username = UPPER (p_user);
-- kill sessions connected for user that needs to be dropped

BEGIN
    FOR rec IN scn_user
    LOOP
        v_count   :=
              v_count
            + 1;
        v_str   :=
               'alter system kill session '''
            || rec.sid
            || ','
            || rec.serial#
            || ''' immediate';

        EXECUTE IMMEDIATE v_str;

--        DBMS_OUTPUT.put_line (v_str);
    END LOOP;

--    DBMS_OUTPUT.put_line (v_count);
END kill_user;
/

DECLARE
    v_user   VARCHAR2 (30) := '&1';
BEGIN
    kill_user (v_user);
END;
/

DROP USER &1 CASCADE;
