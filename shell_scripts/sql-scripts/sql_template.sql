set echo on
set echo off
set verify off
-- set term off
set feedback off
set heading on
set heading off
set pages 10000
set line 10000
set trims on

declare
	p_patient_id number  := 8512; -- william johnston
	p_desired_number number := 200;
begin
	dbms_output.put_line('p_patient_id: '||p_patient_id);
	dbms_output.put_line('p_desired_number: '||p_desired_number);
	--gs_fdb_pkg.test_patient_drug_side_effect_matrix(p_patient_id, p_desired_number);

end;
/
