CREATE USER DBA_UTIL identified by values 'S:50B013EF7E07924D0990B0ECCE0327671F7DADCF75986E65B6C47AFA85FB;816CFAAC3819309E' DEFAULT TABLESPACE DBA_UTIL TEMPORARY TABLESPACE TEMP;
GRANT CTXAPP to DBA_UTIL;
GRANT GS_REST to DBA_UTIL;
GRANT SELECT_CATALOG_ROLE to DBA_UTIL;
GRANT DBA to DBA_UTIL WITH ADMIN OPTION;
GRANT RESOURCE to DBA_UTIL;
GRANT CONNECT to DBA_UTIL;
GRANT CREATE MATERIALIZED VIEW to DBA_UTIL;
GRANT INHERIT ANY PRIVILEGES to DBA_UTIL;
GRANT CREATE TRIGGER to DBA_UTIL;
GRANT CREATE SESSION to DBA_UTIL;
GRANT SELECT ANY DICTIONARY to DBA_UTIL;
GRANT CREATE SEQUENCE to DBA_UTIL;
GRANT CREATE ANY VIEW to DBA_UTIL;
GRANT CREATE VIEW to DBA_UTIL;
GRANT CREATE PROCEDURE to DBA_UTIL;
GRANT UNLIMITED TABLESPACE to DBA_UTIL WITH ADMIN OPTION;
GRANT CREATE DATABASE LINK to DBA_UTIL;
GRANT CREATE SYNONYM to DBA_UTIL;
GRANT CREATE TABLE to DBA_UTIL;
GRANT ALTER SYSTEM to DBA_UTIL;
GRANT ON COMMIT REFRESH to DBA_UTIL;
GRANT GLOBAL QUERY REWRITE to DBA_UTIL;
GRANT SELECT ANY TABLE to DBA_UTIL;
GRANT SELECT on SYS.USER$ to DBA_UTIL;
GRANT SELECT on SYS.DEPENDENCY$ to DBA_UTIL;
GRANT SELECT on SYS.V_$SESSION to DBA_UTIL;
GRANT SELECT on SYS.V_$LOCK to DBA_UTIL;
GRANT SELECT on SYS._CURRENT_EDITION_OBJ to DBA_UTIL;
GRANT SELECT on SYS.DISK_AND_FIXED_OBJECTS to DBA_UTIL;
GRANT EXECUTE on SYS.UTL_HTTP to DBA_UTIL;
GRANT EXECUTE on SYS.DBMS_PIPE to DBA_UTIL;
GRANT EXECUTE on SYS.DBMS_CRYPTO to DBA_UTIL;
GRANT EXECUTE on SYS.DBMS_METADATA to DBA_UTIL;
GRANT EXECUTE on SYS.UTL_MAIL to DBA_UTIL;
GRANT READ on  DIRECTORY EXTERNAL_TABLES to DBA_UTIL;
GRANT WRITE on	DIRECTORY EXTERNAL_TABLES to DBA_UTIL;
GRANT EXECUTE on CTXSYS.CTX_DDL to DBA_UTIL;
GRANT DELETE on GSAUDIT.AUDIT_DDL to DBA_UTIL;
GRANT SELECT on GSAUDIT.AUDIT_DDL to DBA_UTIL;
GRANT UPDATE on GSAUDIT.AUDIT_DDL to DBA_UTIL;
GRANT INHERIT PRIVILEGES on SYS.SYS to DBA_UTIL;
