drop user gsmaster cascade;
drop user fdbprod cascade;
drop user formulary cascade;
drop user rxnorm cascade;

drop user hcpcs cascade;
drop user radlex cascade;
drop user snomed cascade;
exit
