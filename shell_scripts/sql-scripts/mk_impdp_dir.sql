set verify off

drop directory &1;
create directory &1 as '&2';
grant read,write on directory "&1" to public;
exit
