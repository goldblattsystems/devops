alter user &1 identified by "&1";
grant execute on ctx_ddl to &1;
grant execute on sys.utl_mail to &1;
grant execute on sys.utl_smtp to &1;
grant execute on sys.dbms_crypto to &1;
grant execute on dbms_pipe to &1;
grant connect to &1;
exec DBMS_NETWORK_ACL_ADMIN.add_privilege ('gs_rest.xml',upper('&1'), TRUE,'connect');
connect &1/&1@&2
create database link firstdb connect to fdbprod identified by "fdbprod" using '&2';
create database link radlex connect to radlex  identified by "radlex" using '&2';
create database link snomed connect to snomed identified by "snomed" using '&2';
create database link gsmaster connect to gsmaster identified by "gsmaster" using '&2';
create database link sme connect to gsmaster identified by "gsmaster" using '&2';
create database link gsdict connect to system identified by "B1gr3d4gs" using '&2';
create database link gsap connect to system identified by "B1gr3d4gs" using '&2';
create database link gstest connect to system identified by "B1gr3d4gs" using '&2';
create database link gsprod connect to system identified by "B1gr3d4gs" using '&2';
create database link gsbkup connect to system identified by "B1gr3d4gs" using '&2';
exit

