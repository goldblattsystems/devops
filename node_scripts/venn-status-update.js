#!/usr/bin/env node
const axios = require('axios')
var program = require('commander')

console.log('starting',__filename)
const jobStatusConfig = {
    STATUS:'CREATED',
    STATUS_MESSAGE:'no status message provided'
}
const version = '0.0.1'
program
  .version(version)
  .option('--callback-url <callback_url>', 'rest url for venn status')
  .option('--message <message>', 'message to present', 'no message')
  .option('--status <status>', 'status (CREATED, PENDING, IN_PROGRESS, COMPLETED, CANCELLED, FAILURE)')
  .option('--percent-complete <percent>', 'percent complete 0-100', parseInt)
  .option('--job-name <job_name>', 'Jenkins Job Name [required]')
  .option('--job-guid <job_guid>', 'Jenkins Job Guid [required]')
  .option('--build-number <n>','Jenkins BUILD_NUMBER', parseInt)


program.on('--help', function() {
    console.log('')
    console.log('Examples:');
    console.log(`  $ ${__filename} --job-guid <job_guid> --message "some message here" --status IN_PROGRESS --percent-complete 35 --job-name VENN-webdeploy --callback-url http://venn.ad.goldblattsystems.com:8080/venn/rest/jenkins/jobstatus --build-number 51`);
    console.log(`  $ ${__filename} --help`);
    
  })

program.parse(process.argv); 

const vennStatus = (config) => {
  const params = config
  const myconfig ={
    url:config.CALLBACK_STATUS_URL,
    method: 'post',
    params: params
}
axios(myconfig)
.then((res)=>console.log(`res=${res.data}`))
.catch((err)=>console.log(`ouch, err=${err}`))
}

console.log('program',program)
if (program.message) jobStatusConfig.STATUS_MESSAGE=program.message
if (program.status) jobStatusConfig.STATUS=program.status
if (program.percentComplete) jobStatusConfig.PERCENT_COMPLETE=program.percentComplete
if (program.buildNumber) jobStatusConfig.BUILD_NUMBER=program.buildNumber
if (program.jobGuid) jobStatusConfig.JOB_GUID=program.jobGuid
if (program.jobName) jobStatusConfig.JOB_NAME=program.jobName
console.log("BEFORE:program.callbackUrl="+program.callbackUrl)
if (program.callbackUrl) jobStatusConfig.CALLBACK_STATUS_URL=program.callbackUrl
console.log("HOOYAH, jobStatusConfig.CALLBACK_STATUS_URL="+jobStatusConfig.CALLBACK_STATUS_URL)
vennStatus(jobStatusConfig)


