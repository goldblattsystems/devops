#!/usr/bin/env node
const axios = require('axios')
var program = require('commander')
//console.log('starting',__filename)
const emailConfig = {
  subject:'no subject',
  message:'no message',
  vennRestUrl:`http://venn.ad.goldblattsystems.com:8080/venn/rest/email/`
}
const version = '0.0.1'
program
  .version(version)
  .option('--to <to_email>', 'email address of destination', 'javapda@gmail.com')
  .option('--to-alias <alias>', 'alias of destination')
  .option('--from <from_email>', 'email address of sender', 'jkroub@goldblattsystems.com')
  .option('--from-alias <from_alias>', 'alias of sender')
  .option('--subject <subject>', 'Subject line for email', 'no subject provided')
  .option('--message <message_content>', 'body of the email', 'no message in this email')
  .option('--venn-rest-url <rest-url>', 'rest url for email on venn', `${emailConfig.vennRestUrl}`)


program.on('--help', function() {
    console.log('')
    console.log('Examples:');
    console.log(`  $ ${__filename} --to jkroub@goldblattsystems.com --subject "My email subject" --message "My email message"`);
    console.log(`  $ ${__filename} --help`);
    console.log(`  $ ${__filename} -h`);
  })

program.parse(process.argv); 

emailConfig.to = program.to
if(program.toAlias)emailConfig.toAlias=program.toAlias
emailConfig.from = program.from
if(program.fromAlias)emailConfig.fromAlias=program.fromAlias
if(program.subject)emailConfig.subject=program.subject
if(program.message)emailConfig.message=program.message
if(program.vennRestUrl)emailConfig.vennRestUrl = program.vennRestUrl

const email=function(emailConfig) {
  if (! emailConfig.to) {
    throw new Error (`emailConfig messing 'to': config=${emailConfig}`)
  }
   //const url=`http://venn.ad.goldblattsystems.com:8080/venn/rest/email/`
  console.log('emailConfig:',emailConfig)
  axios.get(
    emailConfig.vennRestUrl
    ,
    {
      timeout: 2000,
      params:emailConfig
    }
    )
  .then(function(resp) {
    if (resp) {
        console.log(resp)
      } else {
         console.log('missing environment')
      }

  })
  .catch(function(error){
    console.log(`Ouch got an error ${error}`)
  })

}
email(emailConfig)