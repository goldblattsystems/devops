const {nexusRepo} = require('nexus-repo')

myconfig = {command: "latest-snapshot-meta", 
"g": "com.gs.portal",
"a": "gs-portal",
"mimeType" : "application/json",
"repoUrl": "http://gs-nexus.ad.goldblattsystems.com/nexus"
}
nexusRepo(myconfig) 
.then((result)=>{
  let version=result.version;
  if (version.indexOf('-')>=0) {
    version=version.substring(0,version.indexOf('-'))
  }  
console.log(version)
}, (err)=>{
  console.log(`error ${err}`)
})

