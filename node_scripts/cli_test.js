#!/usr/bin/env node
var program = require('commander');

const version = '0.0.1'


console.log('here we are: '+program);
program
  .version(version)
  .option('-h, --host', 'tomcat server', 'dev-master')
  .option('-p, --port <n>', 'tomcat port number', parseInt)
  .option('--manifest-version', 'Version from manifest (e.g. 7.7.0-SNAPSHOT)')
  .option('-c, --cheese [type]', 'Add the specified type of cheese [marble]', 'marble')
  //.parse(process.argv);

program.on('--help', function() {
    console.log('')
    console.log('Examples:');
    console.log(`  $ ${__filename} --help`);
    console.log(`  $ ${__filename} -h`);
  })
  
program.parse(process.argv);  
  
console.log('you ordered a pizza with:');
if (program.port) console.log(`${program.port}  - port`);
if (program.host) console.log(`${program.host}  - host`);

console.log('later')