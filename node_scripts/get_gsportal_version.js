const axios = require('axios')
const host='dev-master'
const url=`http://${host}:8080/gs-portal/info/manifestjson`
const config={
   responseType: 'json',
   headers: {'X-Requested-With': 'XMLHttpRequest'},
}
axios.get(url,config)
.then(function(resp) {
  if (resp.data.data) {
    if (resp.data.data['Implementation-Version']) {
       const version=resp.data.data['Implementation-Version'].trim()
       console.log(version)
    } else {
       console.log('missing version information')
    }

  }
})
.catch(function(error) {
  console.err(error)
})
