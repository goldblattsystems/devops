
var oracledb = require('oracledb');
var dbConfig = require('./dbconfig');

console.log("dbConfig:  "+JSON.stringify(dbConfig));

const sqlplusConnectStrings = ({sdlc_domain}) => {
  console.log("sdlc_domain: "+sdlc_domain);
  if (sdlc_domain == undefined) {
    throw Error('missing sdlc_domain');
  }
  // Get a non-pooled connection
  oracledb.getConnection(
    {
      user          : dbConfig.user,
      password      : dbConfig.password,
      connectString : dbConfig.connectString
    },
    function(err, connection) {
      if (err) {
        console.error(err.message);
        return;
      }
      connection.execute(
        // The statement to execute
        `select DB, SCHEMA, SDLC_DOMAIN_NAME, APP_DOMAIN, USE, DESCRIPTION, APP_ENV, PASSWORD from DBA_UTIL.SDLC_DEPLOYMENT_INFO where sdlc_domain = ${sdlc_domain}`,
        [],

        // execute() options argument.  Since the query only returns one
        // row, we can optimize memory usage by reducing the default
        // maxRows value.  For the complete list of other options see
        // the documentation.
        { //maxRows: 10
          //,
          outFormat: oracledb.OBJECT  // query result format
          //, extendedMetaData: true      // get extra metadata
          //, fetchArraySize: 100         // internal buffer allocation size for tuning
        },

        // The callback function handles the SQL execution results
        function(err, result) {
          if (err) {
            console.error(err.message);
            doRelease(connection);
            return;
          }
          //console.log(result.metaData); 
          //console.log(result.rows);     
          for (var i in result.rows) {
              const row = result.rows[i];
              console.log(row['SCHEMA']+'/'+row['PASSWORD']+'@'+row['DB']);
          }
          doRelease(connection);
        });
    });

  // Note: connections should always be released when not needed
  function doRelease(connection) {
    connection.close(
      function(err) {
        if (err) {
          console.error(err.message);
        }
      });
  }
  
};


// sqlplusConnectStrings({sdlc_domain:1})
// sqlplusConnectStrings({sdlc_domain:2})
// sqlplusConnectStrings({sdlc_domain:3})
//sqlplusConnectStrings({sdlc_domainBOGUS:3})
//sqlplusConnectStrings({sdlc_domainBOGUS:3})
module.exports = {
  sqlplusConnectStrings
}
