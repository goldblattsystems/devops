#!/usr/bin/env node
const kafka = require('kafka-node')
var Client = kafka.Client;
console.log('kafka here in ',__filename)
const client = new kafka.KafkaClient({kafkaHost: 'teamview.gs.local:9092'})
const Producer = kafka.Producer,
    KeyedMessage = kafka.KeyedMessage,
    producer = new Producer(client),
    km = new KeyedMessage('key', 'message'),
    payloads = [
        { topic: 'topic1', messages: 'hi', partition: 0 },
        { topic: 'topic1', messages: 'Frankenstein', partition: 0 },
        { topic: 'topic2', messages: ['hello', 'world', km] }
    ];
producer.on('ready', function () {
    producer.send(payloads, function (err, data) {
        console.log('DATA HERE:',data);
    });
});
 
producer.on('error', function (err) {
    console.log('in the on error function, err=',err)
})
