const kafka = require('kafka-node')
var ConsumerGroup = kafka.ConsumerGroup;
var consumerOptions = {
    kafkaHost: 'teamview.gs.local:9092',
    //groupId: 'ExampleTestGroup',
    sessionTimeout: 15000,
    // protocol: ['roundrobin'],
    //autoCommit: true,
    //fromOffset: 'earliest' // equivalent of auto.offset.reset valid values are 'none', 'latest', 'earliest'
    fromOffset: 'latest' // equivalent of auto.offset.reset valid values are 'none', 'latest', 'earliest'
  };
  var topics = ['topic1'];  
  var consumerGroup = new ConsumerGroup(Object.assign({id: 'consumer1'}, consumerOptions), topics);
  //consumerGroup.on('error', onError);
//  consumerGroup.on('message', onMessage);
consumerGroup.on('message', function (message) {
    console.log('got a bite')
    console.log(message);
    consumerGroup.commit(function(err, data) {
        console.log('committing now')
        if (err) {
            console.log('ERROR:',err)
        } else {
            console.log('DATA',data)
        }
    });
    consumerGroup.close(true, function() {
        process.exit();
    })

  });

  consumerGroup.on('error', function (err) {
    console.log('in the on error function, err=',err)
})
  
console.log('ConsumerGroup')