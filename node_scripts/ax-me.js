const axios = require('axios')

const url = `http://venn.ad.goldblattsystems.com:8080/venn/rest/jenkins/jobstatus`
const params = {
    JOB_GUID:'c5aa7486-912e-4bdc-9140-6db63513d920',
    JOB_NAME:'charlie-job',
    BUILD_NUMBER : '342',
    PERCENT_COMPLETE:88,
    STATUS_MESSAGE:'wilma was here with betty',
    STATUS:'IN_PROGRESS' // COMPLETED, FAILURE, IN_PROGRESS
}

// https://github.com/axios/axios#axiosposturl-data-config
const config ={
    url:url,
    method: 'post',
    params: params
}
axios(config)
.then((res)=>console.log(`res=${res.data}`))
.catch((err)=>console.log(`ouch, err=${err}`))