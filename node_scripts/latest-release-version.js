const {nexusRepo} = require('nexus-repo')

myconfig = {command: "latest-release-meta", 
"g": "com.gs.portal",
"a": "gs-portal",
"mimeType" : "application/json",
"repoUrl": "https://nexus.goldblattsystems.com/nexus"
}
nexusRepo(myconfig) 
.then((result)=>{
console.log(`${result.version}`)
}, (err)=>{
  console.log(`error ${err}`)
})

