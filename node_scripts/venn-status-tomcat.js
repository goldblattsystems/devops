#!/usr/bin/env node
const axios = require('axios')
var program = require('commander')


console.log('starting',__filename)
const tomcatConfig = {
    TOMCAT_HOST: 'dev-release',
    TOMCAT_USERNAME: '?user',
    TOMCAT_PASSWORD: '?pw',
    TOMCAT_COMMAND: '?tc'
}

const version = '0.0.1'
program
  .version(version)
  .option('--tomcat-host <tomcat-host>', 'tomcat server name', 'dev-master')
  .option('--tomcat-username <tomcat-username>', 'tomcat manager user id', 'tomcat')
  .option('--tomcat-password <tomcat-password>', 'tomcat manager user password', 'bigred')
  .option('--tomcat-command <tomcat-command>', 'tomcat command (status)', 'status')

program.on('--help', function() {
    console.log('')
    console.log('Examples:');
    console.log(`  $ ${__filename} `);
    console.log(`  $ ${__filename} --help`);
  })

program.parse(process.argv); 

if (program.tomcatHost) tomcatConfig.TOMCAT_HOST = program.tomcatHost
if (program.tomcatUsername) tomcatConfig.TOMCAT_USERNAME = program.tomcatUsername
if (program.tomcatPassword) tomcatConfig.TOMCAT_PASSWORD = program.tomcatPassword
if (program.tomcatCommand) tomcatConfig.TOMCAT_COMMAND = program.tomcatCommand


console.log(tomcatConfig)

const tomcatCommand = (config) => {
    console.log('in tomcatComman',config)
    // http://venn.ad.goldblattsystems.com:8080/venn/rest/launchJob/tomcat/command?TOMCAT_HOST=dev-newdev&TOMCAT_USERNAME=tomcat&TOMCAT_PASSWORD=bigred&TOMCAT_COMMAND=status
    axios.post('http://venn.ad.goldblattsystems.com:8080/venn/rest/launchJob/tomcat/command',config)
    .then(function (response) {
        console.log('THE RESPONSE',response)
      })
      .catch(function (error) {
        console.log(error)
      });
}
tomcatCommand(tomcatConfig)