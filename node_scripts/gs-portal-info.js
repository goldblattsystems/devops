#!/usr/bin/env node
const axios = require('axios')
var program = require('commander');
const config={
  responseType: 'json',
  headers: {'X-Requested-With': 'XMLHttpRequest'},
}    
const version = '0.0.1'
program
  .version(version)
  .option('--manifest', 'report META-INF/MANIFEST.MF of project')
  .option('--manifest-version', 'Version from manifest (e.g. 7.7.x-SNAPSHOT)')
  .option('--manifest-timestamp', 'Timestamp from manifest (e.g. 2018-10-29T03:36:15Z)')
  .option('--db-info', 'database information')
  .option('--environment', 'get environment')
  .option('--headers', 'show headers')
  .option('--appcontext <appcontext>', 'override app context','gs-portal')
  .option('-h, --host <host>', 'tomcat server', 'dev-master')
  .option('-p, --port <n>', 'tomcat port number', parseInt)

program.on('--help', function() {
    console.log('')
    console.log('Examples:');
    console.log(`  $ ${__filename} --manifest-version ...could output 7.7.0-SNAPSHOT`);
    console.log(`  $ ${__filename} --manifest ...dumps contents of MANIFEST.MF`);
    console.log(`  $ ${__filename} --help`);
    console.log(`  $ ${__filename} -h`);
  })

program.parse(process.argv); 

const environment = function() {
  const url=`${config.baseUrl}/info/envjson`
  axios.get(
    url,
    config
    )
  .then(function(resp) {
    if (resp) {
        console.log(resp)
      } else {
         console.log('missing environment')
      }

  })
}

const headers = function() {
  const url=`${config.baseUrl}/info/headersjson`
  axios.get(
    url,
    config
    )
  .then(function(resp) {
    if (resp) {
        console.log(resp)
      } else {
         console.log('missing environment')
      }

  })
}  

const manifestTimestamp = function() {
  const url=`${config.baseUrl}/info/manifestjson`
  axios.get(
    url,
    config
    )
  .then(function(resp) {
    if (resp.data.data) {
      if (resp.data.data['timestamp']) {
        const version=resp.data.data['timestamp'].trim()
        console.log(version)
      } else {
         console.log('missing timestamp information')
      }

    }
})
.catch(function(error) {
console.log('start error')
console.log(error)
console.log('end error')
})
}
const pomVersion = function() {
    const url=`${config.baseUrl}/info/manifestjson`
    axios.get(
      url,
      config
      )
    .then(function(resp) {
      if (resp.data.data) {
        if (resp.data.data['Implementation-Version']) {
          const version=resp.data.data['Implementation-Version'].trim()
          console.log(version)
        } else {
           console.log('missing version information')
        }

      }
})
.catch(function(error) {
  console.log('start error')
  console.log(error)
  console.log('end error')
})
}

const manifest = function() {
  const url=`${config.baseUrl}/info/manifestjson`
  axios.get(
    url,
    config
    )
  .then(function(resp) {
    if (resp.data.data) {
      console.log(resp.data.data)
    }
  })
    .catch(function(error) {
    console.log(error)
  })

}

const dbInfo = function() {
  const url=`${config.baseUrl}/info/corepropertiesjson`
  axios.get(
    url,
    config
    )
  .then(function(resp) {
    if (resp.data) {
      //console.log(resp.data)
      console.log(`schema=${resp.data['JdbcUsername']}, url=${resp.data['JdbcUrl']}`)
    }
  })
    .catch(function(error) {
    console.log(error)
  })

}

config.host = (program.host) ? (program.host) : 'dev-master'
config.port = (program.port) ? (program.port) : 8080
config.appcontext = (program.appcontext) ? (program.appcontext) : 'gs-portalarosso'
config.baseUrl = `http://${config.host}:${config.port}/${config.appcontext}`
//console.log(config)

if (program.dbInfo) {
  dbInfo()
}

if (program.manifestVersion) {
  pomVersion()
}
if (program.manifestTimestamp) {
  manifestTimestamp()
}
if (program.manifest) {
  manifest()
}
  if (program.environment) {
    environment()    
  }
  if (program.headers) {
    headers()
  }
