const {TomcatManager} = require('tomcat-manager-api')
const program = require('commander')
const packageJson = require('../package.json')
program
  .version(packageJson.version)
  .option('--server-url <tomcat-server-and-port>', 'tomcat server and port number (e.g. http://dev-release:8080)', 'http://dev-release:8080')
  .option('--tomcat-command <tomcat-command>', 'command to tomcat manager', 'list-apps')
  .option('--username <tomcat-mgr-username>', 'username', 'tomcat')
  .option('--password <tomcat-mgr-password>', 'password', 'bigred')
  .option('--filter-prefix <app-name-starts-with-prefix>', 'only show apps that start with this prefix', 'gs-')
  .option('--verbose','verbose mode')

program.on('--help', function() {
    console.log('')
    console.log('Examples:')
    console.log(`  $ ${__filename} `)
    console.log(`  $ ${__filename} --server-url http://dev-master:8080`)
    console.log(`  $ ${__filename} --help`)
  })

program.parse(process.argv)
const config = {}
if (program.serverUrl) config.serverUrl = program.serverUrl
if (program.tomcatCommand) config.tomcatCommand = program.tomcatCommand
if (program.username) config.username = program.username
if (program.password) config.password = program.password
if (program.filterPrefix) config.filterPrefix = program.filterPrefix

const runTomcat = (config) => {
// --------------------------------
  const listApps = (tr) => {
    tr.listApps()
    .then((tomcatInfo)=>{
        let apps = tomcatInfo.apps.sort( (l,r)=> {
            if (l.name < r.name)
                return -1;
            if (l.name > r.name)
                return 1;
            return 0;
        });
        if(config.filterPrefix) {
            apps = apps.filter( item=>item.name.indexOf(config.filterPrefix)==0)
        }
        for (var i in apps) {
            const item = apps[i]
            console.log(`${item.name}:${item.state}`)
        }
    })
    .catch((err)=>console.log(`problem, err=${err}`))

  }

    var j = new TomcatManager()
    .serverUrl(config.serverUrl)
    .username(config.username)
    .password(config.password)

    const commands = {
        "list-apps" : listApps
    }
    if (!config.tomcatCommand) {
        throw `no tomcat command provided`
    }
    if (commands[config.tomcatCommand]) {
        commands[config.tomcatCommand](j)
    } else {
        throw `no tomcat command of "${config.tomcatCommand}"`
    }
}

runTomcat(config)