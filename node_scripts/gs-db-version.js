#!/usr/bin/env node
// Node-oracledb installation instructions: https://oracle.github.io/node-oracledb/INSTALL.html
var oracledb = require('oracledb');
var program = require('commander');

const version = '0.0.1'

const dbConfig = {
    user: 'clienttest',
    password: 'clienttest',
    connectString: 'GSTEST'
}
function doRelease(connection) {
    connection.close(
      function(err) {
        if (err) {
          console.error(err.message);
        }
      });
  }

const runit = (dbConfig)=>{
  // console.log('runit')
  // console.log(`dbConfig: ${dbConfig}`)
  // console.log(`dbConfig.user: ${dbConfig.user}`)
oracledb.getConnection(
    {
        user          : dbConfig.user,
        password      : dbConfig.password,
        connectString : dbConfig.connectString
      },
      function(err, connection) {
        if (err) {
            console.error(err.message);
            return;
          }
        connection.execute(
            `select VALUE from CODE_VALUE_LIST where CODE = 'VERSION_NUMBER'`,
            [],
            { //maxRows: 10
              outFormat: oracledb.OBJECT  // query result format
              //, extendedMetaData: true      // get extra metadata
              //, fetchArraySize: 100         // internal buffer allocation size for tuning
            },
            function(err, result) {
                if (err) {
                console.error(err.message);
                doRelease(connection);
                return;
                }
                for (var i in result.rows) {
                    const row = result.rows[i];
                    //console.log('row',row)
                    console.log(row['VALUE'])
                }
                doRelease(connection);
            })
      }
)
}
program
  .version(version)
  .option('-h --host <host>','tomcat host', 'dev-master')
  .option('--list-hosts','list all the hosts')
  

program.on('--help', function() {
    console.log('')
    console.log('Examples:');
    console.log(`  $ ${__filename} --help`);
    console.log(`  $ ${__filename} --host dev-release`);
  })
  
program.parse(process.argv);  
const host = (program.host)

const dbdata= {
  'client-test' : {
    user: 'clienttest',
    password: 'clienttest',
    connectString: 'GSTEST'
  },
  'dev-release' : {
    user: 'dev_release',
    password: 'dev_release',
    connectString: 'GSTEST'
  },
  'dev-master' : {
    user: 'dev_master',
    password: 'dev_master',
    connectString: 'GSTEST'
  },
  'dev-newdev' : {
    user: 'dev_newdev',
    password: 'dev_newdev',
    connectString: 'GSTEST'
  },
  'test-release' : {
    user: 'test_release',
    password: 'test_release',
    connectString: 'GSPROD'
  },
  'test-release-2' : {
    user: 'test_release',
    password: 'test_release',
    connectString: 'GSPROD'
  },
  'test-master' : {
    user: 'test_master',
    password: 'test_master',
    connectString: 'GSTEST'
  },
  'test-master-2' : {
    user: 'test_master',
    password: 'test_master',
    connectString: 'GSTEST'
  },
  'test-newdev' : {
    user: 'test_newdev',
    password: 'test_newdev',
    connectString: 'GSTEST'
  },
  'gs-prod' : {
    user: 'emprod',
    password: 'emprod',
    connectString: 'GSPROD'
  },
  'gs-prod-scheduler' : {
    user: 'emprod',
    password: 'emprod',
    connectString: 'GSPROD'
  },
  'gs-demobox' : {
    user: 'gsdemo',
    password: 'gsdemo',
    connectString: 'GSPROD'
  },
  'sme' : {
    user: 'sme',
    password: 'sme',
    connectString: 'GSDICT'
  },
  'kahuna1' : {
    user: 'kahuna1',
    password: 'kahuna1',
    connectString: 'GSBKUP'
  },
  'kahuna3' : {
    user: 'kahuna3',
    password: 'kahuna3',
    connectString: 'GSBKUP'
  },
}

if (program.listHosts) {
  console.log('listing hosts:')
  const hosts = []
  console.log(Object.keys(dbdata).sort( (l,r)=>l.localeCompare(r)))
  return
}

// console.log(`host: ${host}`)
// console.log(`dbdata: ${dbdata}`)



runit(dbdata[host])
