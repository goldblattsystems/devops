#!/usr/bin/env node

var oracledb = require('oracledb');
var dbConfig = require('./dbconfig');
var {DEPLOYMENT_TYPE} = require('./gs-constants');
var {sqlplusConnectStrings} = require('./sqlplus_login_info');

sqlplusConnectStrings({sdlc_domain:DEPLOYMENT_TYPE.MASTER.SDLC_DOMAIN});