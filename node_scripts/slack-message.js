#!/usr/bin/env node
const axios = require('axios')
var program = require('commander')
var _ = require('underscore')

const config = {
}
/*
curl -X POST --data-urlencode "payload={\"color\":\"${SLACK_COLOR}\", \"channel\": \"${SLACK_CHANNEL}\", \"username\": \"${SLACK_USER}\", \"text\": \"${SLACK_MESSAGE}\", \"icon_emoji\": \"${SLACK_ICON}\" ${SLACK_FIELDS}}" ${WEBHOOK_URL}

*/
const slackIcons = [
    ':helmet_with_white_cross:',
    ':mask:',
    ':bellhop_bell:',
    ':+1:',
    ':heavy_check_mark:',
    ':grinning:',
    ':warning:',
    ':clap:',
    ':ok:',
    ':ok_hand:',
    ':dart:',
    ':birthday:',
    ':desktop_computer:',
    ':thumbsup_all:',
    ':battery:',
    ':large_orange_diamond:',
    ':male-detective:',
    ':man-running:',
    ':man-walking:',
    ':moyai:',
    ':shield:',
    ':snowman_without_snow:',
    ':man_dancing:',
    ':dancer:',
    ':woman-running:',
    ':eye:',
    ':zzz:',
    ':bomb:',
    ':mortar_board:',
    ':tophat:',
    ':coffee:',
    ':alarm_clock:',
    ':clock12:',
    ':clock1:',
    ':clock2:',
    ':clock3:',
    ':clock4:',
    ':clock5:',
    ':clock6:',
    ':clock7:',
    ':clock8:',
    ':clock9:',
    ':clock10:',
    ':clock11:',

    ':clock1230:',
    ':clock130:',
    ':clock230:',
    ':clock330:',
    ':clock430:',
    ':clock530:',
    ':clock630:',
    ':clock730:',
    ':clock830:',
    ':clock930:',
    ':clock1030:',
    ':clock1130:',

]
const version = '0.0.1'
program
  .version(version)
  .option('--slack-channel <slack-channel>', 'slack channel (#venn, @john)')//, '@john')
  .option('--slack-channel-list <slack-channel-list>', 'comma-separated list of slakc channels (#venn, #treatment)')
  .option('--slack-icon <slack-icon>', `icon to use (${slackIcons[0]})`, ':mask:')// ':large_orange_diamond:')
  .option('--slack-color <slack-color>', 'color to use (good, danger, warning, #35a64f)', 'good')
  .option('--slack-message <slack-message>', 'slack message', 'no message')
  .option('--slack-user <slack-user>', 'user to log message as', 'vennadmin')
  .option('--show-icons','message has all icons in it')
  .option('--webhook-url <webhook-url>', 'api url', 'https://hooks.slack.com/services/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')
  .option('--resources','lists resources')
  .option('--verbose','lists resources')

program.on('--help', function() {
    console.log('')
    console.log('Examples:')
    console.log(`  $ ${__filename} `)
    console.log(`  $ ${__filename} --slack-channel-list "#venn,#treatment" --slack-message "Testing :mask: only"`)
    console.log(`  $ ${__filename} --help`)
  })

program.parse(process.argv)

if (program.webhookUrl) config.webhookUrl = program.webhookUrl
if (program.slackMessage) config.text = program.slackMessage
if (program.showIcons) config.text += slackIcons
if (program.slackUser) config.username = program.slackUser
if (program.slackChannel) config.channel = program.slackChannel
if (program.slackChannelList) config.channelList = program.slackChannelList.split(',').map(function(x){ return x.trim();})
if (program.slackIcon) config.icon_emoji = program.slackIcon
if (program.slackColor) config.color = program.slackColor
if (program.verbose) config.verbose = true
if (program.resources) {
    console.log(`
      Slack Webhooks : https://api.slack.com/incoming-webhooks
    `)
}
const sendToSlack = function(config) {
    axios.post(config.webhookUrl, config)
    .then(function(response){
        if (config.verbose) {
        console.log('response:',response)
        }
    })
    .catch(function(error){
        console.log(error)
    });  
}

if (config.verbose) {
    console.log('config',config)
}
if (config.channel) {
    sendToSlack(config)
}

if (config.channelList) {
     
    for (var channel in config.channelList) {
        const copy = _.clone(config)
        copy.channel = config.channelList[channel]
        console.log('slack message to', copy.channel)
        sendToSlack(copy)
    }
} 